// declare ambient module for scss files to ignore TS errors
declare module '*.scss';
// below are modules that don't have type definitions

// next-routes has typings but CJS require() make tsc get the type incorrectly
declare module 'next-routes';
// workaround withRedux(MakeStore) type errors
declare type MakeStore = (initialState: any, options: any) => any;

// declare new property for the `window` global object
interface Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  BASE_DOMAIN: string;
  ENV: string;
  _googCsa: Function;
}
