import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import compression from 'compression';
import next from 'next';
import routes from './src/routes';
import nextConfig from './next.config';

const dev = process.env.NODE_ENV !== 'production';

const app = next({
  dev,
  dir: path.resolve(__dirname, '.'),
  conf: nextConfig,
});

// custom dynamic routes - https://github.com/fridays/next-routes
const handler = routes.getRequestHandler(app, ({ req, res, route, query }) => {
  app.render(req, res, route.page, query);
});

const port = process.env.PORT || 3000;

app.prepare().then(() => {
  const server = express();
  server.use(express.static(`${__dirname}/public`));
  server.use(compression());
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(cookieParser());

  server.get('/health-check', (req, res) => {
    res.json({ ok: true });
  });

  server.use(handler);

  server.listen(port, (err) => {
    if (err) {
      throw err;
    }
    console.log(`> Ready on http://localhost:${port}`);
  });
});
