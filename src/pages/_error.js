import React from 'react';
import PropTypes from 'prop-types';

const Error = ({ statusCode }) => (
  <p style={{ marginTop: '10vh' }}>
    <h1
      style={{
        textAlign: 'center',
        padding: '50px 0',
        color: '#ffba00',
        fontWeight: 'bold',
        fontSize: '40px',
        minHeight: '350px',
      }}
    >
      {statusCode ? `An error ${statusCode} occurred on server` : 'An error occurred on client'}
    </h1>
  </p>
);

Error.getInitialProps = ({ res, err }) => {
  // eslint-disable-next-line no-nested-ternary
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

Error.defaultProps = {
  statusCode: null,
};

Error.propTypes = {
  statusCode: PropTypes.number,
};

export default Error;
