import React, { memo, useRef, useState, useEffect } from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AutoAffix from 'react-overlays/lib/AutoAffix';
import SimilarProduct from '~/features/SimilarProduct/SimilarProduct';
import fakeProduct from '~/fakeData/products';
import ProductLasted from '~/components/ProductViewLasted';
import ProductImage from '~/features/ProductImage/ProductImage';
import ProductInfo from '~/features/ProductInfo/ProductInfo';
import { addToCart } from '~/features/Cart/action';
import { pushRouter } from '~/helpers/router';
import { PATTERN_URL } from '~/constants/routes';
import ProductDescription from '~/features/ProductDescription/ProductDescription';
import { isServer } from '~/helpers/utils';
import ProductComment from '~/features/ProductComment/ProductComment';

const ProductPage = ({ dispatch, isMobile, comment, productInfo }) => {
  const refView = useRef(null);
  const [productLasted, setProductLasted] = useState([]);
  const { title = '', thumbnail = [], images = [], description, productId } = productInfo;
  const [qty, setQty] = useState(1);

  const onChangeQty = ({ target: { value } }) => setQty(parseInt(value, 10));

  const handleAddToCart = (isRedirect) => {
    dispatch(addToCart(productInfo, qty));
    if (isRedirect) {
      const href = PATTERN_URL.SHOPPING_CART;
      pushRouter(href);
    }
  };

  useEffect(() => {
    if (!isServer()) {
      const initState = JSON.parse(localStorage.getItem('productLasted')) || [];
      if (initState.length >= 0) {
        // eslint-disable-next-line prefer-destructuring
        initState[1] = initState[0] || [];
        initState[0] = productInfo;
      }
      localStorage.setItem('productLasted', JSON.stringify(initState));
      setProductLasted(initState);
    }
  }, [productId]);

  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <div className="row no-margin" style={{ paddingTop: '20px' }}>
        <div className="col-md-10 no-padding" ref={refView}>
          <div className="col-md-5 no-padding">
            {isMobile && (
              <h3 style={{ margin: '10px 0', fontSize: '18px', fontWeight: '500' }}>{title}</h3>
            )}
            <ProductImage
              isMobile={isMobile}
              images={images.concat(images)}
              title={title}
              thumbnail={thumbnail}
            />
          </div>
          <div className="col-md-7">
            <ProductInfo
              isMobile={isMobile}
              productInfo={productInfo}
              onChangeQty={onChangeQty}
              qty={qty}
            />
            <div style={{ display: 'flex', justifyContent: ' space-between', marginTop: '20px' }}>
              <button
                type="button"
                className="btn-main"
                style={{ padding: '15px 25px' }}
                onClick={() => handleAddToCart(true)}
              >
                Mua ngay
              </button>
              <button
                type="button"
                className="btn-main"
                style={{ padding: '15px 25px' }}
                onClick={() => handleAddToCart()}
              >
                Thêm giỏ hàng
              </button>
            </div>
          </div>
          <div className="col-md-12 no-padding">
            <ProductDescription description={description} />
          </div>
          <div className="col-md-12 no-padding">
            <ProductComment comment={comment} />
          </div>
        </div>
        {!isMobile && (
          <div className="col-md-2" style={{ paddingRight: 0 }}>
            <AutoAffix viewportOffsetTop={50} container={refView?.current}>
              <div>
                <ProductLasted products={productLasted} />
                <div
                  style={{
                    height: '250px',
                    width: '100%',
                    marginBottom: '10px',
                    background: 'red',
                    opacity: '0.3',
                  }}
                />
              </div>
            </AutoAffix>
          </div>
        )}
      </div>
      <SimilarProduct title="Sản phẩm tương tự" products={fakeProduct.slice(0, 10)} />
    </>
  );
};

ProductPage.getInitialProps = async ({ store, query, asPath, location }) => ({
  store,
  query,
  asPath,
  location,
});

ProductPage.defaultProps = {
  comment: [],
  productInfo: fakeProduct.slice(0, 1)[0],
};

ProductPage.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  comment: PropTypes.arrayOf(),
  productInfo: PropTypes.objectOf(),
};

const mapStateToProps = ({ device }) => ({
  isMobile: device.isMobile,
});

export default connect(mapStateToProps, null)(memo(ProductPage));
