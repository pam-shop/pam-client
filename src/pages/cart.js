import React, { memo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import MyCart from '~/features/MyCart';
import Label from '~/components/Label';
import ButtonShopping from '~/components/ButtonShopping';
import { pushRouter } from '~/helpers/router';

const ShoppingCartPage = ({ shoppingCart }) => {
  const { cart } = shoppingCart;
  const handleLogin = () => {};

  const clickEmptyCart = () => {
    pushRouter('/');
  };

  return cart.length > 0 ? (
    <>
      <Label
        text="Bạn ơi đăng nhập để lưu lại thông tin nhé !"
        button="Đăng nhập"
        onClick={handleLogin}
      />
      <MyCart shoppingCart={shoppingCart} />
    </>
  ) : (
    <div style={{ textAlign: 'center' }}>
      <h1
        style={{
          padding: '50px 0',
          color: '#ffba00',
          fontWeight: 'bold',
          fontSize: '40px',
        }}
      >
        Giỏ hàng trống
      </h1>
      <ButtonShopping onClick={clickEmptyCart} label="Tiếp tục mua sắm" />
    </div>
  );
};

ShoppingCartPage.propTypes = {
  shoppingCart: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = ({ shoppingCart }) => ({
  shoppingCart,
});

export default connect(mapStateToProps, null)(memo(ShoppingCartPage));
