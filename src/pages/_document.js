/* eslint-disable react/no-danger */
import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import { env, app } from '~/config';

// eslint-disable-next-line react/jsx-props-no-spreading
const renderAppleMetas = () => app.head.meta.map((item) => <meta {...item} />);

export default class ServerDocument extends Document {
  static async getInitialProps(ctx) {
    const documentProps = await super.getInitialProps(ctx);
    return { ...documentProps };
  }

  render() {
    return (
      <html lang="vi">
        <Head>
          <link rel="shortcut icon" href="/favicon.ico" />
          {renderAppleMetas()}
        </Head>
        <body>
          {this.props.customValue}
          <Main />
          <script
            dangerouslySetInnerHTML={{
              __html: `window.ENV = "${env}"; `,
            }}
          />
          <NextScript />
        </body>
      </html>
    );
  }
}
