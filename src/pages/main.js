import React, { memo, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect, useDispatch } from 'react-redux';
import AutoAffix from 'react-overlays/lib/AutoAffix';
import fakeProduct from '~/fakeData/products';
import Products from '~/features/Product/Product';
import MainFilter from '~/features/MainFilter/MainFilter';
import { addToCart } from '~/features/Cart/action';
import { setFilterPrice } from '~/features/MainFilter/action';
import { isServer } from '~/helpers/utils';

const MainPage = ({ location, query }) => {
  const dispatch = useDispatch();
  const [hasMore, setHasMore] = useState(true);
  const [products, setProducts] = useState(() => fakeProduct);
  const refProduct = useRef(null);
  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
  };

  const loadFunc = () => {
    if (products.length > 100) {
      setHasMore(false);
    }
    setTimeout(() => {
      setProducts(products.concat(fakeProduct));
    }, 500);
  };

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'column', width: '25%' }}>
        <AutoAffix viewportOffsetTop={50} container={refProduct?.current}>
          <div>
            <MainFilter query={query} pathname={location?.pathname} />
          </div>
        </AutoAffix>
      </div>
      <div style={{ width: '75%', marginBottom: '100px' }}>
        <Products
          products={products}
          loadFunc={loadFunc}
          hasMore={hasMore}
          addToCart={handleAddToCart}
        />
      </div>
    </div>
  );
};

MainPage.getInitialProps = async ({ store, query, asPath, location }) => {
  const { dispatch } = store;
  const { price = null } = query;
  if (isServer()) {
    if (price) {
      dispatch(
        setFilterPrice({
          min: price.split('-')[0],
          max: price.split('-')[1],
        })
      );
    }
  }

  return {
    store,
    query,
    asPath,
    location,
  };
};

MainPage.defaultProps = {};

MainPage.propTypes = {
  location: PropTypes.instanceOf(Object).isRequired,
  query: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = ({ device }) => ({
  isMobile: device.isMobile,
});

export default connect(mapStateToProps, null)(memo(MainPage));
