import React, { memo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const PaymentPage = ({ isMobile }) => (
  <h1
    style={{
      textAlign: 'center',
      padding: '50px 0',
      color: '#ffba00',
      fontWeight: 'bold',
      fontSize: '40px',
      minHeight: '350px',
    }}
  >
    Trang đang được phát triển - {isMobile} - payment
  </h1>
);

PaymentPage.getInitialProps = async ({ store, query, asPath, location }) =>
  // const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
  ({
    store,
    query,
    asPath,
    location,
  });

PaymentPage.defaultProps = {};

PaymentPage.propTypes = {
  isMobile: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ device }) => ({
  isMobile: device.isMobile,
});

export default connect(mapStateToProps, null)(memo(PaymentPage));
