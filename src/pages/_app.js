// @ts-nocheck
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import PamLayout from '~/hocs/withPamLayout';
import { initStore } from '~/store';
import getInitProps from '~/helpers/initProps';
import config from '~/config';
import { UserProvider } from '~/hooks/ClientContext';
import NextNProgress from '~/components/ProgressBar';
import { loadInitCart } from '~/features/Cart/action';
// import MessengerChat from '~/components/FbMessager';

const MyApp = ({ Component, appProps, pageProps, store }) => {
  const { location, query } = appProps;
  const { dispatch } = store;

  useEffect(() => {
    dispatch(loadInitCart());
  }, []);

  return (
    <Provider store={store}>
      <UserProvider value={{ config }}>
        <Head>
          <title>{config.app.title}</title>
        </Head>
        <PamLayout router={{ ...location, ...query }} isMobile={appProps.isMobile}>
          <>
            {/* <MessengerChat pageId={config.fbId} ref="fb-msgr" />; */}
            <div id="fb-root" />
            <NextNProgress />
            <Component {...appProps} {...pageProps} />
          </>
        </PamLayout>
      </UserProvider>
    </Provider>
  );
};

MyApp.getInitialProps = async ({ Component, ctx }) => {
  const appProps = getInitProps(ctx);
  let pageProps = {};
  const componentGetInitialProps = Component?.WrappedComponent?.type?.getInitialProps;
  if (componentGetInitialProps && typeof componentGetInitialProps === 'function') {
    ctx.config = config;
    ctx.query = appProps.query;
    ctx.location = appProps.location;
    pageProps = await componentGetInitialProps(ctx);
  }
  return {
    pageProps,
    appProps,
  };
};

MyApp.defaultProps = {
  pageProps: {},
  appProps: {},
};

MyApp.propTypes = {
  appProps: PropTypes.instanceOf(Object),
  pageProps: PropTypes.instanceOf(Object),
  Component: PropTypes.instanceOf(Object).isRequired,
  store: PropTypes.instanceOf(Object).isRequired,
};

export default withRedux(initStore)(MyApp);
