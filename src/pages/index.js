import React, { memo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Banner from '~/features/Banner';
import HotProducts from '~/features/HotProducts';
import banner from '~/fakeData/banner';
import fakeProduct from '~/fakeData/products';

const HomePage = ({ isMobile, banners, hotProducts }) => (
  <>
    <Banner isMobile={isMobile} banners={banners} />
    <HotProducts hotProducts={hotProducts} />
  </>
);

HomePage.getInitialProps = async ({ store, query, asPath, location }) => ({
  store,
  query,
  asPath,
  location,
});

const mapStateToProps = ({ device }) => ({
  isMobile: device.isMobile,
  device,
});

HomePage.defaultProps = {
  banners: banner,
  hotProducts: fakeProduct,
};

HomePage.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  banners: PropTypes.instanceOf(Array),
  hotProducts: PropTypes.instanceOf(Array),
};

export default connect(mapStateToProps, null)(memo(HomePage));
