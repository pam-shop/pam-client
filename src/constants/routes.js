const ROUTE_NAME = {
  PRODUCT_VIEW_FULL: '#productViewFull',
  PRODUCT_VIEW: '#productView',
  HOME: '#home',
  HOME_FULL: '#homeFull',
  SHOPPING_CART: '#shoppingCart',
  ERROR: '#errorView',
  CATEGORY: '#categoryView',
  SUB_CATEGORY: '#subCategoryView',
  ABOUT: '#about',
  CONTACT: '#concat',
  SHOPPING_GUIDE: '#shoppingGuide',
  PAYMENT: '#payment',
};

const PAGE_NAME = {
  PRODUCT_VIEW: 'productView',
  HOME: 'index',
  SHOPPING_CART: 'cart',
  ABOUT: 'about',
  CONTACT: 'contact',
  SHOPPING_GUIDE: 'shoppingGuide',
  CATEGORY: 'main',
  PAYMENT: 'payment',
};

const PATTERN_URL = {
  PRODUCT_VIEW_FULL: '/:category/:subcategory/:productId',
  PRODUCT_VIEW: '/chi-tiet-san-pham/:productId',
  SUB_CATEGORY: '/:category/:subcategory',
  CATEGORY: '/:category',
  HOME_FULL: '/trang-chu',
  SHOPPING_CART: '/gio-hang',
  ABOUT: '/gioi-thieu',
  CONTACT: '/lien-he',
  SHOPPING_GUIDE: '/huong-dan-mua-hang',
  LOGIN: '/thanh-toan',
  PAYMENT: '/thanh-toan',
  HOME: '/',
};

// pls add to route params if you add more param on Pattern url
const ROUTE_PARAMS = ['subcategory', 'category', 'productId'];

export { ROUTE_NAME, PAGE_NAME, PATTERN_URL, ROUTE_PARAMS };
