import { PATTERN_URL, PAGE_NAME } from './routes';

export const VIEW_MODE_PARAM_NAME = 'vm';
export const SPLIT_MARK = '&';
export const EQUAL_MARK = '=';
export const FALSY_VALUE_DENY = ['undefined', 'null', ''];

export const TOP_MENU = [
  {
    name: 'Trang chủ',
    href: PATTERN_URL.HOME_FULL,
    page: '/',
  },
  {
    name: 'Giới thiệu',
    href: PATTERN_URL.ABOUT,
    page: PAGE_NAME.ABOUT,
  },
  {
    name: 'Hướng dẫn mua hàng',
    href: PATTERN_URL.SHOPPING_GUIDE,
    page: PAGE_NAME.SHOPPING_GUIDE,
  },
  {
    name: 'Liên hệ',
    href: PATTERN_URL.CONTACT,
    page: PAGE_NAME.CONTACT,
  },
];

export const BOTTOM_HEADER = [
  {
    name: 'Open time',
    label: '9h - 21h',
    image: '/icon_open.png',
  },
  {
    name: 'Refund',
    label: 'Đổi hàng trong 7 ngày',
    image: '/icon_vong.png',
  },
  {
    name: 'Warranty',
    label: 'Bảo hành sản phẩm',
    image: '/icon_hh.png',
  },
  {
    name: 'Delivery',
    label: 'Giao hàng toàn quốc',
    image: '/icon_giaohang.png',
  },
];

export const FOOTER_GUIDE = [
  {
    name: 'Hướng dẫn mua hàng',
    href: PATTERN_URL.SHOPPING_GUIDE,
  },
  {
    name: 'Chính sách vận chuyển',
    href: PATTERN_URL.SHOPPING_GUIDE,
  },
  {
    name: 'Chính sách bảo mật',
    href: PATTERN_URL.SHOPPING_GUIDE,
  },
  {
    name: 'Chính sách bảo hành',
    href: PATTERN_URL.SHOPPING_GUIDE,
  },
  {
    name: 'Chính sách đổi trả',
    href: PATTERN_URL.SHOPPING_GUIDE,
  },
];

export const FOOTER_MAIN = [
  {
    name: 'Giới thiệu về PAM SHOP',
    href: PATTERN_URL.ABOUT,
  },
  {
    name: 'Quy chế hoạt động',
    href: PATTERN_URL.SHOPPING_GUIDE,
  },
  {
    name: 'Liên hệ',
    href: PATTERN_URL.CONTACT,
  },
  {
    name: 'Tuyển dụng',
    href: PATTERN_URL.SHOPPING_GUIDE,
  },
];

export const PHONE_NUMBER = '070 79 666 79';
