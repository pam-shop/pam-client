const categories = [
  {
    category: '1000',
    name: 'Áo khoác',
    href: 'ao-khoac',
    subCategory: [
      {
        category: '1000',
        subCategory: '1010',
        name: 'Áo khoác dù',
        href: 'ao-khoac-du',
      },
      {
        category: '1000',
        subCategory: '1020',
        name: 'Áo khoác jean',
        href: 'ao-khoac-jean',
      },
      {
        category: '1000',
        subCategory: '1030',
        name: 'Áo khoác nỉ',
        href: 'ao-khoac-nỉ',
      },
      {
        category: '1000',
        subCategory: '1040',
        name: 'Áo vest',
        href: 'ao-vest',
      },
      {
        category: '1000',
        subCategory: '1050',
        name: 'Áo Gile',
        href: 'ao-gile',
      },
      {
        category: '1000',
        subCategory: '1060',
        name: 'Áo khoác kaki',
        href: 'ao-khoac-kaki',
      },
    ],
  },
  {
    category: '2000',
    name: 'Áo thun',
    href: 'ao-thun',
    subCategory: [
      {
        category: '2000',
        subCategory: '2010',
        name: 'Áo thun ba lỗ',
        href: 'ao-thun-ba-lo',
      },
      {
        category: '2000',
        subCategory: '2020',
        name: 'Áo thun cổ tròn',
        href: 'ao-thun-co-tron',
      },
      {
        category: '2000',
        subCategory: '2030',
        name: 'Áo thun cổ tim',
        href: 'ao-thun-co-tim',
      },
      {
        category: '2000',
        subCategory: '2020',
        name: 'Áo thun body',
        href: 'ao-thun-body',
      },
    ],
  },
  {
    category: '3000',
    name: 'Áo sơ mi',
    href: 'ao-so-mi',
    subCategory: [
      {
        category: '3000',
        subCategory: '3010',
        name: 'Áo sơ mi tay dài',
        href: 'ao-so-mi-tay-dai',
      },
      {
        category: '3000',
        subCategory: '3020',
        name: 'Áo sơ mi tay ngắn',
        href: 'ao-so-mi-tay-ngan',
      },
      {
        category: '3000',
        subCategory: '3030',
        name: 'Áo sơ mi ca rô',
        href: 'ao-so-mi-ca-ro',
      },
    ],
  },
  {
    category: '4000',
    name: 'Quần short',
    href: 'quan-short',
    subCategory: [
      {
        category: '4000',
        subCategory: '4010',
        name: 'Quần short jean',
        href: 'quan-short-jean',
      },
      {
        category: '4000',
        subCategory: '4020',
        name: 'Quần short thun',
        href: 'quan-short-thun',
      },
      {
        category: '4000',
        subCategory: '4030',
        name: 'Quần short kaki',
        href: 'quan-short-kaki',
      },
    ],
  },
  {
    category: '5000',
    name: 'Quần dài',
    href: 'quan-dài',
    subCategory: [
      {
        category: '5000',
        subCategory: '5010',
        name: 'Quần jean',
        href: 'quan-jean',
      },
      {
        category: '5000',
        subCategory: '5020',
        name: 'Quần kaki',
        href: 'quan-kaki',
      },
      {
        category: '5000',
        subCategory: '5030',
        name: 'Quần tây',
        href: 'quan-tay',
      },
      {
        category: '5000',
        subCategory: '5040',
        name: 'Quần jogger nỉ',
        href: 'quan-jogger-ni',
      },
      {
        category: '5000',
        subCategory: '5050',
        name: 'Quần jogger thun',
        href: 'quan-jogger-thun',
      },
    ],
  },
  {
    category: '6000',
    name: 'Phụ kiện',
    href: 'phu-kien',
    subCategory: [
      {
        category: '6000',
        subCategory: '6010',
        name: 'Nón',
        href: 'non',
      },
      {
        category: '6000',
        subCategory: '6020',
        name: 'Thắt lưng',
        href: 'that-lung',
      },
      {
        category: '6000',
        subCategory: '6030',
        name: 'Mắt kính',
        href: 'mat-kinh',
      },
      {
        category: '6000',
        subCategory: '6040',
        name: 'Bóp',
        href: 'bop',
      },
      {
        category: '6000',
        subCategory: '6050',
        name: 'Túi xách',
        href: 'tui-xach',
      },
    ],
  },
  {
    category: '7000',
    name: 'Khác',
    href: 'khac',
  },
  {
    category: '8000',
    name: 'Sale',
    href: 'sale',
  },
];

export default categories;
