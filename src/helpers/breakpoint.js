export const BREAK_POINTS = {
  sm: '540px',
  md: '768px',
  gtmd: '769px',
  lg: '940px',
  xl: '941px',
  md_max: '767px',
  gtlg: '992px',
};
export function mediaBreakPointUp(bp, cssStr) {
  const breakPoint = BREAK_POINTS[bp] || BREAK_POINTS.sm;
  return `
    @media (min-width: ${breakPoint}) {
      ${cssStr}
    }
  `;
}
export function mediaBreakPointDown(bp, cssStr) {
  const breakPoint = BREAK_POINTS[bp] || BREAK_POINTS.sm;
  return `
    @media (max-width: ${breakPoint}) {
      ${cssStr}
    }
  `;
}

export function mediaBreakPointRange(bp, bp1, cssStr) {
  const breakPoint = BREAK_POINTS[bp] || BREAK_POINTS.md;
  const breakPoint1 = BREAK_POINTS[bp1] || BREAK_POINTS.gtlg;
  return `
    @media (min-width: ${breakPoint}) and (max-width: ${breakPoint1}) {
      ${cssStr}
    }
  `;
}
