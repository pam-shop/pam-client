export const checkIsMobile = (userAgent) => {
  if (
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
      userAgent
    )
  ) {
    return true;
  }
  return false;
};

export const createNewObject = (target = {}) => {
  if (typeof target !== 'object') return {};
  return JSON.parse(JSON.stringify(target));
};

export const isServer = () => {
  if (typeof window === 'undefined') return true;
  return false;
};

/* eslint-disable no-useless-escape */
const removeUnicode = (str) => {
  let newStr = str;
  if (newStr) {
    newStr = newStr.toLowerCase();
    newStr = newStr.replace(/̀|̣.|΄|̃|̉/g, '');
    newStr = newStr.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    newStr = newStr.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    newStr = newStr.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    newStr = newStr.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    newStr = newStr.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    newStr = newStr.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    newStr = newStr.replace(/đ/g, 'd');
    newStr = newStr.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,
      '-'
    );

    newStr = newStr.replace(/[^a-z0-9\-]/g, '');
    // thay thế 2- thành 1-
    newStr = newStr.replace(/-+-/g, '-');
    newStr = newStr.replace(/^\-+|\-+$/g, '');

    return newStr;
  }
  return '';
};

export const nameToUrl = (name) => removeUnicode(name);

export const queryBuilder = (params) => {
  const arrayParam = [];
  // eslint-disable-next-line array-callback-return
  Object.keys(params).map((key) => {
    if (params[key]) {
      arrayParam.push(key + '=' + params[key]);
    }
  });
  return arrayParam.length > 0 ? '?' + arrayParam.join('&') : '';
};

export const insertDivideToArray = (list) => {
  const result = [];
  const lengthResult = list.length + (list.length - 1);
  let countInserted = 0;
  for (let index = 0; index < lengthResult; index++) {
    if (index % 2 !== 0) {
      result[index] = {
        devider: true,
        key: index,
      };
    } else {
      result[index] = list[countInserted];
      countInserted = countInserted + 1;
    }
  }
  return result;
};

export const isActiveRouter = (router = {}, current = null) => {
  const {
    query: { category = null, subcategory = null },
  } = router;
  if (category === current) {
    return true;
  }
  if (subcategory === current) {
    return true;
  }
  return false;
};

export const buildProductUrl = (productInfo) => {
  let fullAdViewUrl = '';
  let dashboardAdViewUrl;
  ['category_name', 'subcategory_name', 'productId', 'product_code'].forEach((key) => {
    if (productInfo[key]) {
      switch (key) {
        case 'category_name': {
          fullAdViewUrl = `${fullAdViewUrl}/${nameToUrl(productInfo[key])}`;
          break;
        }
        case 'subcategory_name': {
          fullAdViewUrl = `${fullAdViewUrl}/${nameToUrl(productInfo[key])}`;
          break;
        }
        case 'product_code': {
          fullAdViewUrl = `${fullAdViewUrl}--${productInfo[key]}`;
          break;
        }
        default: {
          fullAdViewUrl = `${fullAdViewUrl}/${productInfo[key]}`;
          break;
        }
      }
    } else {
      dashboardAdViewUrl = `/chi-tiet-san-pham/${productInfo.productId}`;
    }
  });
  // fullAdViewUrl = `${fullAdViewUrl}.htm`;
  return dashboardAdViewUrl || fullAdViewUrl;
};

export function formatNumberValue(value) {
  const stringValue = value ? value.toString() : '0';
  const arrString = [];
  for (let i = stringValue.length - 1; i >= 0; i--) {
    if ((stringValue.length - i) % 3 === 0 && i - 1 >= 0) {
      arrString[i] = `.${stringValue[i]}`;
    } else {
      arrString[i] = stringValue[i];
    }
  }
  return arrString.join('');
}

export const getQueryFilter = (query) => {
  if (query.price) {
    return false;
  }
  return true;
};
