import { createNewObject, checkIsMobile, isServer } from './utils';

describe('helpers/utils  - createNewObject', () => {
  test('new object should be created from target object', () => {
    const target = {
      a: 1,
      b: '2',
      c: true,
    };
    const newObject = createNewObject(target);

    expect(newObject).toEqual(target);
    expect(newObject).not.toBe(target);
  });

  test('empty new object should be created from non-object target', () => {
    const target = '{"foo": "bar"}';
    const newObject = createNewObject(target);

    expect(newObject).toEqual({});
  });
});

// prettier-ignore
describe('helpers/utils   - checkIsMobile', () => {
  test('checkIsMobile should return true for mobile userAgent', () => {
    // iOS Safari
    expect(checkIsMobile('Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'))
      .toBe(true);
    // Android Chrome
    expect(checkIsMobile('Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Mobile Safari/537.36'))
      .toBe(true);
    // Samsung Internet
    expect(checkIsMobile('Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-G925F Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36'))
      .toBe(true);
  });
  test('checkIsMobile should return false for desktop userAgent', () => {
    // Firefox on OSX
    expect(checkIsMobile('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:72.0) Gecko/20100101 Firefox/72.0'))
      .toBe(false);
    // Chrome on OSX
    expect(checkIsMobile('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'))
      .toBe(false);
    // Safari
    expect(checkIsMobile('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.4 Safari/605.1.15'))
      .toBe(false);
  });
});

describe('helpers/isServer', () => {
  it('return false on client', () => {
    expect(isServer()).toBe(false);
  });

  it('return true on server', () => {
    // @ts-ignore
    delete global.window;
    expect(isServer()).toBe(true);
  });
});
