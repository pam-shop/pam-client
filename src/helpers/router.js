import routes from '~/routes';

const { Router } = routes;

export const pushRouter = (href) => {
  Router.pushRoute(href, href, { shallow: true });
};
