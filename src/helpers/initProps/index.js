import url from 'url';
import { ROUTE_PARAMS } from '~/constants/routes';
import { checkIsMobile } from '~/helpers/utils';
import { storeMobileDevice } from '~/store/modules/Device/action';

const buildQueryOnClient = (query = {}) => {
  const result = {};
  // remove query duplicate with route params
  const arrayQuery = Object.keys(query).filter((key) => !ROUTE_PARAMS.includes(key));
  arrayQuery.forEach((key) => {
    result[key] = query[key];
  });
  return result;
};

const serverProps = (context) => {
  const {
    req,
    store: { dispatch },
  } = context;
  const userAgent = req.headers['user-agent'];
  const isMobile = checkIsMobile(userAgent);
  const urlObject = url.parse(req.originalUrl);
  dispatch(storeMobileDevice(isMobile));
  return {
    isMobile,
    location: {
      pathname: urlObject.pathname,
    },
    query: req.query,
  };
};

const clientProps = (context) => {
  const { asPath, query } = context;
  const { userAgent } = navigator;
  const isMobile = checkIsMobile(userAgent);
  return {
    isMobile,
    location: {
      pathname: asPath.split('?')[0],
    },
    query: buildQueryOnClient(query),
  };
};

export default (context) => {
  const { isServer } = context;
  const initAppProps = isServer ? serverProps : clientProps;
  return initAppProps(context);
};
