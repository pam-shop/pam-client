import { createNewObject } from '~/helpers/utils';
import formatQuery from './formatQuery';
import { SPLIT_MARK, EQUAL_MARK, FALSY_VALUE_DENY } from '~/constants';

const QUERY_CONFIG = {
  splitMark: SPLIT_MARK,
  equalMark: EQUAL_MARK,
  validateQueryValue: (value) => !FALSY_VALUE_DENY.includes(`${value}`),
  formatQueryKey: (keyString) => {
    const result = keyString.trim();
    const keyValid = result.match(/\b.*\b/);
    if (keyValid) {
      return keyValid[0];
    }
    return result;
  },
};

class QueryParser {
  constructor() {
    this.formatQuery = formatQuery;
  }

  static parse = (string) => {
    const result = {};
    const { splitMark, equalMark, formatQueryKey, validateQueryValue } = QUERY_CONFIG;
    const arrayQuery = string.split(splitMark);
    arrayQuery.forEach((query) => {
      const [key, value] = query.split(equalMark);
      const isValueValid = validateQueryValue(value);
      if (isValueValid) {
        result[formatQueryKey(key)] = value;
      }
    });
    return result;
  };

  static stringify = (object) => {
    const { splitMark, equalMark, formatQueryKey, validateQueryValue } = QUERY_CONFIG;
    const arrayQuery = [];
    Object.keys(object).forEach((key) => {
      const value = object[key];
      const isValueValid = validateQueryValue(value);
      if (isValueValid) {
        arrayQuery.push(`${formatQueryKey(key)}${equalMark}${value}`);
      }
    });
    return arrayQuery.join(splitMark);
  };

  createQueryString = (query = {}) => {
    let newQuery = createNewObject(query);
    if (typeof this.formatQuery === 'function') {
      newQuery = this.formatQuery(newQuery);
    }
    return QueryParser.stringify(newQuery);
  };
}

export default QueryParser;
