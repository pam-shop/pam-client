const queryReplaceFunctions = {
  page: (value) => {
    const pageInt = parseInt(value, 10);
    if (Number.isNaN(pageInt) || pageInt <= 1) {
      return { page: 1 };
    }
    return { o: (pageInt - 1) * 20, page: pageInt };
  },
};

export default (query = {}) => {
  let result = {};
  Object.keys(query).forEach((key) => {
    const value = query[key];
    const queryFunctions = queryReplaceFunctions[key];
    if (typeof queryFunctions === 'function') {
      result = {
        ...result,
        ...queryFunctions(value),
      };
    } else {
      result[key] = value;
    }
  });
  return result;
};
