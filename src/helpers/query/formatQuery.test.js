import formatQuery from './formatQuery';

describe('helpers/query/formatQuery', () => {
  test('formatQuery should return new object params query with out page', () => {
    const query = {
      cg: 5010,
    };

    const expectParams = {
      cg: 5010,
    };

    expect(formatQuery(query)).toEqual(expectParams);
  });

  test('formatQuery should return new object params query', () => {
    const query = {
      page: 5,
    };

    const expectParams = {
      o: 80,
      page: 5,
    };

    expect(formatQuery(query)).toEqual(expectParams);
  });

  test('formatQuery should return new object params query with page = 1', () => {
    const query = {
      page: 1,
    };

    const expectParams = {
      page: 1,
    };

    expect(formatQuery(query)).toEqual(expectParams);
  });

  test('formatQuery should return new object params query with page = 0', () => {
    const query = {
      page: 0,
    };

    const expectParams = {
      page: 1,
    };

    expect(formatQuery(query)).toEqual(expectParams);
  });
});
