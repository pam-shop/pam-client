import React, { memo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '~/components/Header';
import Footer from '~/components/Footer';
import ScrollToTop from '~/components/ScrollToTop';
import MainMenu from '~/features/MainMenu';
import SnackBar from '~/features/SnackBar';
import '../assets/styles/global.scss';

const PamLayout = ({ children, isMobile, router, message }) => (
  <>
    <div className="pam-layout">
      <Header location={router} isMobile={isMobile} />
      <MainMenu isMobile={isMobile} />
      {children}
      {message && <SnackBar />}
      <ScrollToTop />
    </div>
    <Footer />
  </>
);
PamLayout.propTypes = {
  children: PropTypes.element.isRequired,
  isMobile: PropTypes.bool.isRequired,
  router: PropTypes.shape({}).isRequired,
  message: PropTypes.string.isRequired,
};

const mapStateToProps = ({ shoppingCart: { message } }) => ({
  message,
});

export default connect(mapStateToProps, null)(memo(PamLayout));
