// @ts-nocheck
import { createStore, applyMiddleware, compose } from 'redux';
import createThunkMiddleware from '~/store/middlewares/createThunkMiddleware';
import createApiMiddleware from '~/store/middlewares/createApiMiddleware';
import rootReducer from './rootReducer';
import ClientConfig from '~/helpers/api/ClientConfig';
import config from '~/config';

const _initialState = {};

const composeEnhancers =
  (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

export const initStore = (initialState = _initialState, { req, res }) => {
  const clientConfig = new ClientConfig(config, req, res);
  const middlewares = [createThunkMiddleware(clientConfig), createApiMiddleware(clientConfig)];
  const enhancer = composeEnhancers(applyMiddleware(...middlewares));
  return createStore(rootReducer(), initialState, enhancer);
};
