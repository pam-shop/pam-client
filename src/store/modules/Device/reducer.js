import { STORE_MOBILE_DEVICE, STORE_IS_CLIENT } from './action';

const initialState = {
  isMobile: false,
  isClient: false,
};

export default function (state = initialState, action = {}) {
  const actionFactory = {
    [STORE_MOBILE_DEVICE]: {
      ...state,
      isMobile: action.payload,
    },
    [STORE_IS_CLIENT]: {
      ...state,
      isClient: action.payload,
    },
  };

  const nextState = actionFactory[action.type] ?? state;
  return nextState;
}
