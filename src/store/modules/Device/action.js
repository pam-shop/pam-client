export const STORE_MOBILE_DEVICE = 'containers/STORE_MOBILE_DEVICE';
export const STORE_IS_CLIENT = 'containers/STORE_IS_CLIENT';

export const storeMobileDevice = (isMobile) => ({
  type: STORE_MOBILE_DEVICE,
  payload: isMobile,
});

export const storeSetIsClient = (isClient) => ({
  type: STORE_IS_CLIENT,
  payload: isClient,
});
