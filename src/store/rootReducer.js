import { combineReducers } from 'redux';
import device from '~/store/modules/Device/reducer';
import savedProduct from '~/features/SaveProduct/reducer';
import shoppingCart from '~/features/Cart/reducer';
import mainFilter from '~/features/MainFilter/reducer';

export default function rootReducer() {
  return combineReducers({
    device,
    savedProduct,
    shoppingCart,
    mainFilter,
  });
}
