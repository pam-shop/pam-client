import React, { memo } from 'react';
import PropTypes from 'prop-types';
import routes from '~/routes';
import { buildProductUrl } from '~/helpers/utils';

const { Link } = routes;

const style = ({ src }) => ({
  background: `url('${src}') center center no-repeat`,
  width: '100%',
  height: '180px',
  marginBottom: '10px',
});

const styleH4 = {
  display: 'block',
  width: '150px',
  margin: '8px',
  'font-weight': '600',
  'font-size': '14px',
  'text-transform': 'uppercase',
};

const ProductLasted = memo(({ products }) => (
  <>
    <div style={styleH4}>Sản phẩm đã xem</div>
    {products.map((p, i) => {
      const styles = style({ src: p.thumbnail });
      const productUrl = buildProductUrl(p);
      return (
        // eslint-disable-next-line react/no-array-index-key
        <Link route={productUrl} key={`${products.title}-${i}`}>
          <a>
            <div style={styles} />
          </a>
        </Link>
      );
    })}
  </>
));

ProductLasted.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ProductLasted;
