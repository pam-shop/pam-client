import React, { memo, useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import style from './style.scss';

const DebounceInput = ({
  debounceTimeout,
  onSubmit,
  styles,
  className,
  onChange,
  placeHolder,
  isFooter,
}) => {
  const [search, setSearch] = useState('');
  const debounceTimeoutRef = useRef(null);
  const formRef = useRef(null);
  const router = useRouter();

  useEffect(() => {
    const searchCurrent = router?.query?.q ? router.query.q : '';
    // @ts-ignore
    setSearch(searchCurrent);
  }, []);

  const handleOnChange = (e) => {
    const { value } = e.target;
    setSearch(value);

    if (onChange) {
      onChange(value);
    }
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if (!onSubmit) {
      return;
    }

    if (debounceTimeoutRef?.current) {
      clearTimeout(debounceTimeoutRef.current);
    }

    debounceTimeoutRef.current = setTimeout(() => {
      onSubmit(search);
    }, debounceTimeout);
  };

  const handleOnFocus = () => {
    const { current = null } = formRef;
    if (current) {
      current.style.border = '1px solid #ffba00';
    }
  };
  const handleOnBlur = () => {
    const { current = null } = formRef;
    if (current) {
      current.style.border = '1px solid #f4f4f4';
    }
  };

  return (
    <form
      onSubmit={handleOnSubmit}
      onFocus={handleOnFocus}
      onBlur={handleOnBlur}
      className={style.debounceForm}
      ref={formRef}
    >
      <input
        style={styles}
        className={className}
        value={search}
        ref={debounceTimeoutRef}
        type="text"
        placeholder={placeHolder}
        onChange={handleOnChange}
      />
      {!isFooter && (
        <div className={style.debounceFormWrapperButton}>
          <button aria-label="Search" type="button" tabIndex={0} onClick={handleOnSubmit}>
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
              <g fill="none" fillRule="evenodd">
                <path
                  stroke="#FE9900"
                  strokeWidth="1.3"
                  d="M13.59 13.56a6.334 6.334 0 0 1-4.494 1.858 6.33 6.33 0 0 1-4.494-1.858A6.319 6.319 0 0 1 2.74 9.077a6.317 6.317 0 0 1 1.862-4.483 6.335 6.335 0 0 1 4.494-1.856 6.331 6.331 0 0 1 4.494 1.857 6.317 6.317 0 0 1 1.862 4.482 6.317 6.317 0 0 1-1.862 4.483zm.797.25l.318-.41a6.999 6.999 0 0 0 1.486-4.322 7.053 7.053 0 0 0-2.078-5.005A7.073 7.073 0 0 0 9.096 2a7.067 7.067 0 0 0-5.018 2.073A7.053 7.053 0 0 0 2 9.077a7.054 7.054 0 0 0 2.078 5.005 7.072 7.072 0 0 0 5.017 2.073 7.034 7.034 0 0 0 4.327-1.478l.412-.317 5.54 5.526a.388.388 0 0 0 .551 0 .386.386 0 0 0 0-.55l-5.538-5.525z"
                />
                <path stroke="#FFF" strokeWidth=".1" d="M1 1h20v20H1z" opacity=".01" />
              </g>
            </svg>
          </button>
        </div>
      )}
    </form>
  );
};

DebounceInput.defaultProps = {
  debounceTimeout: 300,
  onSubmit: () => {},
  onChange: () => {},
  className: '',
  styles: {},
  placeHolder: 'Tìm kiếm',
  isFooter: false,
};

DebounceInput.propTypes = {
  debounceTimeout: PropTypes.number,
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
  className: PropTypes.string,
  styles: PropTypes.instanceOf(Object),
  placeHolder: PropTypes.string,
  isFooter: PropTypes.bool,
};

export default memo(DebounceInput);
