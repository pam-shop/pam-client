import React, { memo } from 'react';
import routes from '~/routes';
import style from './style.scss';
import FooterRegister from '~/features/RegisterMail';
import { FOOTER_MAIN, FOOTER_GUIDE } from '~/constants';

const { Link } = routes;

const Footer = () => (
  <div style={{ display: 'inline-block', width: '100%' }}>
    <FooterRegister />
    <div className={style.layoutFooter}>
      <div className="pam-layout">
        <div className={style.wrapperFooter}>
          <div className={style.footerMain}>
            <h3>Thông tin</h3>
            {FOOTER_MAIN.map((item) => (
              <Link route={item.href} key={item.name}>
                <a>{item.name}</a>
              </Link>
            ))}
          </div>
          <div className={style.footerMain}>
            <h3>Hướng dẫn</h3>
            {FOOTER_GUIDE.map((item) => (
              <Link route={item.href} key={item.name}>
                <a>{item.name}</a>
              </Link>
            ))}
          </div>
          <div className={style.connectWithUs}>
            <h3>Liên kết với chúng tôi</h3>
            <a>
              <img src="/fb.png" alt="fb" />
            </a>
            <a>
              <img src="/instagram.png" alt="instagram" />
            </a>
          </div>
        </div>
      </div>
    </div>
    <div className={style.copyright}>
      Copyright © 2020
      <Link route="/">
        <a>
          <span>PAM STORE</span>
        </a>
      </Link>
    </div>
  </div>
);

export default memo(Footer);
