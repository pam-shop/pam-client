import React, { memo } from 'react';
import style from './style.scss';

const Caret = () => <span className={style.caret} />;

export default memo(Caret);
