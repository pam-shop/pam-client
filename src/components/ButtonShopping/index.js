import React, { memo } from 'react';
import PropTypes from 'prop-types';

const ButtonShopping = ({ onClick, label }) => (
  <div onClick={onClick} role="presentation" className="btn-main m-auto" style={{ width: '40%' }}>
    {label}
  </div>
);

ButtonShopping.propTypes = {
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
};

export default memo(ButtonShopping);
