import React, { memo } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';
import { pushRouter } from '~/helpers/router';
import { queryBuilder } from '~/helpers/utils';
import DebounceInput from '~/components/DebounceInput';
import Cart from '~/features/Cart';
import Account from '~/features/Account';

const MainHeader = ({ location: { pathname, query } }) => {
  const onSubmit = (value) => {
    const newQuery = {
      ...query,
      q: value,
      page: '1',
    };
    const href = `${pathname}${queryBuilder(newQuery)}`;
    pushRouter(href);
  };

  const onChange = (value) => {
    if (value === '') {
      pushRouter(pathname);
    }
  };

  return (
    <div className={style.mainHeader}>
      <div className={style.mainHeaderSearch}>
        <DebounceInput placeHolder="Tìm kiếm sản phẩm" onChange={onChange} onSubmit={onSubmit} />
      </div>
      <div role="presentation" onClick={() => pushRouter('/')} className={style.mainHeaderLogo}>
        PAM SHOP
      </div>
      <div className={style.mainHeaderAction}>
        <Account />
        <Cart />
      </div>
    </div>
  );
};

MainHeader.propTypes = {
  // isMobile: PropTypes.bool.isRequired,
  location: PropTypes.instanceOf(Object).isRequired,
};

export default memo(MainHeader);
