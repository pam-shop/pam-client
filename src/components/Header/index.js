import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import MainHeader from './MainHeader';
import style from './style.scss';
import { BOTTOM_HEADER, TOP_MENU, PHONE_NUMBER } from '~/constants';
import routes from '~/routes';

const { Link } = routes;

const Header = ({ isMobile, location }) => {
  const router = useRouter();

  if (isMobile) {
    return null;
  }

  return (
    <>
      <div className={style.topHeader}>
        <div className={style.topHeaderHotLine}>Hotline : {PHONE_NUMBER}</div>
        <div className={style.topHeaderHeadMenu}>
          <ul>
            {TOP_MENU.map(({ name, href, page }) => {
              const pageName = page.startsWith('/') ? page : `/${page}`;
              const isActive = router?.pathname === pageName;
              const styleActive = {
                color: isActive ? '#333333' : '#9d9d9d',
              };
              return (
                <li key={name}>
                  <Link route={href}>
                    <a style={styleActive}>{name}</a>
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <MainHeader location={location} />
      <div className={style.bottomHeader}>
        {BOTTOM_HEADER.map(({ label, image }) => (
          <div className={style.bottomHeaderItem} key={label}>
            <img src={image} alt={label} />
            <span>{label}</span>
          </div>
        ))}
      </div>
    </>
  );
};

Header.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  location: PropTypes.instanceOf(Object).isRequired,
};

export default memo(Header);
