import React, { memo } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';

const Label = ({ text, button, onClick, type }) => (
  <div className={`${style[type]} ${style.common}`}>
    {text}
    {button && (
      <button type="button" onClick={onClick} className={`btn btn-main ${style.button}`}>
        {button}
      </button>
    )}
  </div>
);

Label.defaultProps = {
  button: '',
  onClick: () => {},
  type: 'warrning',
};

Label.propTypes = {
  text: PropTypes.string.isRequired,
  button: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.string,
};

export default memo(Label);
