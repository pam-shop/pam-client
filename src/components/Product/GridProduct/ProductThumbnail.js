import React, { memo } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';
import routes from '~/routes';

const { Link } = routes;

const handleErrorImage = (e) => {
  const image = e.target;
  image.style.display = 'none';
};

const AdThumbnail = ({
  image,
  title,
  numberOfImages,
  ribbonAd,
  productUrl,
  buttonSaveProduct,
  isSale,
}) => (
  <div className={style.thumbnailWrapper}>
    <Link route={productUrl}>
      <a className={style.thumbnailImg}>
        {image && (
          <img alt={title} onError={handleErrorImage} height="300px" src={image} loading="lazy" />
        )}
        {image !== '' && numberOfImages > 1 && (
          <div className={style.imageNum}>
            <span className={style.text}>{numberOfImages}</span>
          </div>
        )}
        {ribbonAd && ribbonAd}
      </a>
    </Link>
    {isSale && (
      <div className={style.saleTag}>
        <img src="/sale.png" alt="sale" />
      </div>
    )}
    {buttonSaveProduct && buttonSaveProduct}
  </div>
);

AdThumbnail.defaultProps = {
  numberOfImages: 0,
  image: '',
  title: '',
  ribbonAd: null,
  isSale: false,
};

AdThumbnail.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  numberOfImages: PropTypes.number,
  ribbonAd: PropTypes.oneOfType([() => null, PropTypes.object]),
  productUrl: PropTypes.string.isRequired,
  buttonSaveProduct: PropTypes.element.isRequired,
  isSale: PropTypes.bool,
};

export default memo(AdThumbnail);
