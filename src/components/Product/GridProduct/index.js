import React, { memo } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';
import ProductThumbnail from './ProductThumbnail';
import { buildProductUrl } from '~/helpers/utils';
import SaveProductButton from '~/features/SaveProduct';
import ProductFooter from './ProductFooter';

const GridProduct = ({ productInfo, addToCart, showProducts, isShowCart, isSlider }) => {
  const {
    images,
    title,
    thumbnail,
    productId,
    isSale,
    priceString,
    priceSaleString,
    discount,
    isFreeShipping,
  } = productInfo;

  const productUrl = buildProductUrl(productInfo);
  const styleShow = showProducts === 3 ? style.gridProduct3 : '';
  const styleSlider = isSlider ? style.gridProductSilder : '';
  return (
    <div className={`${style.gridProduct} ${styleShow} ${styleSlider}`}>
      <div className={style.wrapperLink}>
        {isShowCart && (
          <div className={style.addToCart} role="presentation" onClick={addToCart}>
            <img src="/addcart.png" alt="add-to-cart" />
          </div>
        )}
        <ProductThumbnail
          image={thumbnail}
          title={title}
          numberOfImages={images && images.length}
          productUrl={productUrl}
          isSale={isSale}
          buttonSaveProduct={<SaveProductButton productId={productId} />}
        />
        <ProductFooter
          productUrl={productUrl}
          title={title}
          priceSaleString={priceSaleString}
          priceString={priceString}
          isSale={isSale}
          isFreeShipping={isFreeShipping}
          discount={discount}
          isSlider={isSlider}
        />
      </div>
    </div>
  );
};

GridProduct.defaultProps = {
  showProducts: 3,
  isShowCart: true,
  isSlider: false,
};

GridProduct.propTypes = {
  productInfo: PropTypes.instanceOf(Object).isRequired,
  addToCart: PropTypes.func.isRequired,
  showProducts: PropTypes.number,
  isShowCart: PropTypes.bool,
  isSlider: PropTypes.bool,
};

export default memo(GridProduct);
