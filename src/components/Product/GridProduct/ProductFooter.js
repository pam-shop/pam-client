import React, { memo } from 'react';
import PropTypes from 'prop-types';
import routes from '~/routes';
import style from './style.scss';

const { Link } = routes;

const AdBody = ({
  productUrl,
  title,
  priceString,
  isSale,
  priceSaleString,
  isFreeShipping,
  discount,
  isSlider,
}) => {
  const styleHasSale = isSale
    ? {
        color: 'grey',
        fontSize: '13px',
        fontWeight: '500',
        textDecoration: 'line-through',
      }
    : {};

  const _renderPrice = () => (
    <div className={style.wrapperPrice}>
      {isSale && priceSaleString && (
        <span className={style.priceSale}>
          {priceSaleString}
          {discount && <span className={style.discount}>-{discount}</span>}
        </span>
      )}
      <span style={styleHasSale} className={style.price}>
        {priceString}
      </span>
    </div>
  );

  return (
    <div className={style.footerWrapper}>
      <Link route={productUrl}>
        <a className={style.title}>
          {isFreeShipping && !isSlider && (
            <span>
              <img src="/free-shipping.png" alt="free-shipping" />
            </span>
          )}
          <span> {title}</span>
        </a>
      </Link>
      {!isSlider && _renderPrice()}
    </div>
  );
};

AdBody.defaultProps = {
  priceSaleString: '',
  title: '',
  isSale: false,
  priceString: '',
  isFreeShipping: false,
  isSlider: false,
  discount: '',
};

AdBody.propTypes = {
  priceString: PropTypes.string,
  title: PropTypes.string,
  productUrl: PropTypes.string.isRequired,
  isSale: PropTypes.bool,
  priceSaleString: PropTypes.string,
  isFreeShipping: PropTypes.bool,
  isSlider: PropTypes.bool,
  discount: PropTypes.string,
};

export default memo(AdBody);
