import React, { useState, useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';

const ScrollToTop = ({ showUnder }) => {
  const [show, setShow] = useState(false);

  const handleScroll = () => {
    if (window.pageYOffset > showUnder) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  const onClick = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const showStyle = show ? style.visible : style.invisible;

  return (
    <div role="button" tabIndex={0} className={`${style.Wrapper} ${showStyle}`} onClick={onClick}>
      <div className={style.scrollTop}>
        <i />
      </div>
    </div>
  );
};

ScrollToTop.propTypes = {
  showUnder: PropTypes.number,
};

ScrollToTop.defaultProps = {
  showUnder: 1000,
};

export default memo(ScrollToTop);
