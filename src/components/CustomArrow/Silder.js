/* eslint-disable react/jsx-props-no-spreading */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Style from './style.scss';

const CustomArrowCom = ({ onClick, next }) => {
  const label = next ? 'left' : 'right';
  return (
    <button
      onClick={onClick}
      className={`${Style.controlButton} ${Style[label]}`}
      aria-label={label}
      tabIndex={0}
      type="button"
    >
      <img
        alt={label}
        src={`https://static.chotot.com/storage/chotot-icons/svg/${label}-chevron.svg`}
      />
    </button>
  );
};

CustomArrowCom.defaultProps = {
  next: false,
};

CustomArrowCom.propTypes = {
  next: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

const CustomArrow = memo(CustomArrowCom);

export default CustomArrow;
