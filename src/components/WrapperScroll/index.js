import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Style from './style.scss';

export default class WrapperScroll extends PureComponent {
  state = {
    isShowIconNext: false,
    isShowIconPrev: false,
  };

  static propTypes = {
    widthScroll: PropTypes.number,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
      .isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    widthScroll: null,
    className: '',
  };

  componentDidMount() {
    this.refWrapper.addEventListener('scroll', this.handleScroll);
    const { clientWidth, scrollWidth } = this.refWrapper;
    if (scrollWidth > clientWidth) {
      this.setState({ isShowIconNext: true });
    }
  }

  componentWillUnmount() {
    this.refWrapper.removeEventListener('scroll', this.handleScroll);
  }

  onClick = (e, isScrollLeft) => {
    const { widthScroll } = this.props;
    const { childNodes, scrollWidth, scrollLeft } = this.refWrapper;
    const numberScroll = widthScroll || Math.round(scrollWidth / childNodes.length) + 100;

    if (isScrollLeft) {
      this.refWrapper.scrollTo({
        left: scrollLeft - numberScroll,
        behavior: 'smooth',
      });
    } else {
      this.refWrapper.scrollTo({
        left: scrollLeft + numberScroll,
        behavior: 'smooth',
      });
    }
  };

  handleScroll = (e) => {
    const {
      //          dynamic     maxWidth    showWidth
      target: { scrollLeft, scrollWidth, clientWidth },
    } = e;

    const { isShowIconPrev, isShowIconNext } = this.state;
    const maxWidthScroll = scrollWidth - clientWidth;

    if (maxWidthScroll === scrollLeft) {
      if (isShowIconNext) {
        this.setState({ isShowIconNext: false });
      }
    } else {
      if (!isShowIconNext) {
        this.setState({ isShowIconNext: true });
      }
    }

    if (scrollLeft > 0) {
      if (!isShowIconPrev) {
        this.setState({ isShowIconPrev: true });
      }
    } else {
      if (isShowIconPrev) {
        this.setState({ isShowIconPrev: false });
      }
    }
  };

  render() {
    const { children, className } = this.props;
    const { isShowIconPrev, isShowIconNext } = this.state;
    if (children) {
      return (
        <div className={Style.wrapperOverflow}>
          <div
            ref={(ref) => {
              this.refWrapper = ref;
            }}
            className={className}
          >
            {children}
          </div>
          <i
            className={`${Style.iconPrev} ${isShowIconPrev && Style.active}`}
            onClick={(e) => this.onClick(e, true)}
            tabIndex={0}
            role="button"
            aria-label="Prev"
          />
          <i
            className={`${Style.iconNext} ${isShowIconNext && Style.active}`}
            onClick={(e) => this.onClick(e)}
            tabIndex={0}
            role="button"
            aria-label="Next"
          />
        </div>
      );
    }
    return <div />;
  }
}
