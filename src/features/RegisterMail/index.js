import React, { memo } from 'react';
import DebounceInput from '~/components/DebounceInput';
import style from './style.scss';

const FooterRegister = () => {
  const onChange = () => {};

  const onSubmit = () => {};

  return (
    <div className={style.registerMail}>
      <div className="pam-layout">
        <img src="/mail_footer.png" alt="register mail" />
        <div className={style.wrapperInput}>
          <DebounceInput
            isFooter
            placeHolder="Đăng kí nhận thông tin khuyến mãi"
            onChange={onChange}
            onSubmit={onSubmit}
          />
          <button onClick={onSubmit} type="button" className="btn-main">
            Đăng ký
          </button>
        </div>
      </div>
    </div>
  );
};

export default memo(FooterRegister);
