import React, { memo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import GridProduct from '~/components/Product/GridProduct';
import { addToCart } from '~/features/Cart/action';

const HotProduct = ({ hotProducts, dispatch }) => {
  if (!hotProducts || hotProducts.length === 0) {
    return null;
  }

  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
  };

  return (
    <>
      <h1
        style={{
          textAlign: 'center',
          margin: '15px 0',
          textTransform: 'uppercase',
          color: '#ffba00',
          fontSize: '28px',
          fontWeight: 'bold',
          letterSpacing: '3px',
        }}
      >
        Sản phẩm nổi bật
      </h1>
      <div style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'row' }}>
        {hotProducts.slice(0, 8).map((product) => (
          <React.Fragment key={product.id}>
            <GridProduct
              showProducts={4}
              productInfo={product}
              addToCart={() => handleAddToCart(product)}
            />
          </React.Fragment>
        ))}
      </div>
    </>
  );
};

HotProduct.propTypes = {
  hotProducts: PropTypes.instanceOf(Array),
  dispatch: PropTypes.func.isRequired,
};

HotProduct.defaultProps = {
  hotProducts: [],
};

export default connect(null, null)(memo(HotProduct));
