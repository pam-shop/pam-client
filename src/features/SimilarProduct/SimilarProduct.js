/* eslint-disable react/jsx-props-no-spreading */
import React, { memo } from 'react';
import dynamic from 'next/dynamic';
import PropTypes from 'prop-types';
import GridProduct from '~/components/Product/GridProduct';
import { addToCart } from '~/features/Cart/action';
import style from './style.scss';
import CustomArrow from '~/components/CustomArrow/Silder';

const Slider = dynamic(() => import('react-slick'), {
  loading: () => <div />,
  ssr: false,
});

const settings = {
  dots: false,
  arrow: true,
  prevArrow: <CustomArrow />,
  nextArrow: <CustomArrow next />,
  autoplaySpeed: 2000,
  speed: 300,
  autoplay: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  infinite: true,
  swipe: false,
};

const SimilarProduct = ({ isError, title, products }) => {
  if (isError) {
    return null;
  }

  if (products && products?.length === 0) {
    return <div />;
  }

  return (
    <>
      {title && <h1 className={style.title}>{title}</h1>}
      <Slider {...settings}>
        {products.map((product) => (
          <React.Fragment key={product.id}>
            <GridProduct
              showProducts={4}
              productInfo={product}
              addToCart={() => addToCart(product)}
              isShowCart={false}
              isSlider
            />
          </React.Fragment>
        ))}
      </Slider>
    </>
  );
};

SimilarProduct.defaultProps = {
  isError: false,
  title: 'Sản phẩm tương tự',
  products: [],
};

SimilarProduct.propTypes = {
  isError: PropTypes.bool,
  title: PropTypes.string,
  products: PropTypes.arrayOf(PropTypes.object),
};

export default memo(SimilarProduct);
