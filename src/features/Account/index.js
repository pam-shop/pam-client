import React, { memo } from 'react';
import PropTypes from 'prop-types';
import routes from '~/routes';
import style from './style.scss';
import { PATTERN_URL } from '~/constants/routes';

const { Link } = routes;

const Account = ({ isFixed }) => {
  const styles = {
    marginTop: isFixed ? '-2px' : '',
    display: isFixed ? 'block' : '',
    marginRight: isFixed ? '0' : '',
  };
  return (
    <div className={style.Login} style={styles}>
      <Link route={PATTERN_URL.LOGIN}>
        <a>
          <svg
            className={`${isFixed && 'no-margin'}`}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 30 30"
          >
            <g fill="none" fillRule="evenodd" stroke="#000" strokeWidth="1.2">
              <circle cx="15" cy="15" r="14.4" />
              <circle cx="15" cy="12" r="4.5" />
              <path d="M25.442 24.63c-1.453-4.536-5.579-7.807-10.442-7.807-4.932 0-9.106 3.365-10.502 8" />
            </g>
          </svg>
          {!isFixed && <span>Đăng nhập </span>}
        </a>
      </Link>
    </div>
  );
};

Account.defaultProps = {
  isFixed: false,
};

Account.propTypes = {
  isFixed: PropTypes.bool,
};

export default memo(Account);
