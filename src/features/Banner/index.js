/* eslint-disable react/jsx-props-no-spreading */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import Route from '~/routes';
import style from './style.scss';

const Slider = dynamic(() => import('react-slick'), {
  loading: () => <div />,
});

const handleErrorImage = (e) => {
  const image = e.target;
  image.style.display = 'none';
};

const { Link } = Route;

const settings = {
  dots: true,
  arrow: true,
  autoplaySpeed: 2000,
  speed: 700,
  autoplay: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  swipe: false,
};

const Banner = ({ banners }) => {
  if (!banners || banners.length === 0) {
    return null;
  }

  const element = banners.map((item) => (
    <div className={style.bannerDesktopItem} key={`${item.href} - ${Date.now()}`}>
      <Link route={item.href}>
        <a>
          <img src={item.image} alt={item.name} onError={handleErrorImage} />
        </a>
      </Link>
    </div>
  ));

  return (
    <div className={style.bannerDesktop}>
      <Slider {...settings}>{element}</Slider>
    </div>
  );
};

Banner.propTypes = {
  banners: PropTypes.instanceOf(Array),
  isMobile: PropTypes.bool.isRequired,
};

Banner.defaultProps = {
  banners: [],
};

export default memo(Banner);
