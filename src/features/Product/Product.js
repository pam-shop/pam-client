import React, { memo } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import GridProduct from '~/components/Product/GridProduct';

const Product = ({ withPaginate, products, loadFunc, hasMore, addToCart }) => {
  if (withPaginate) {
    return (
      <div style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'row' }}>
        {products.map((product) => (
          <React.Fragment key={product.id}>
            <GridProduct
              showProducts={3}
              productInfo={product}
              addToCart={() => addToCart(product)}
            />
          </React.Fragment>
        ))}
      </div>
    );
  }

  return (
    <div>
      <InfiniteScroll pageStart={0} loadMore={loadFunc} hasMore={hasMore} loader={<div />}>
        <div style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'row' }}>
          {products.map((product) => (
            <React.Fragment key={product.id}>
              <GridProduct
                showProducts={3}
                productInfo={product}
                addToCart={() => addToCart(product)}
              />
            </React.Fragment>
          ))}
        </div>
      </InfiniteScroll>
    </div>
  );
};

Product.defaultProps = {
  withPaginate: false,
  hasMore: false,
  loadFunc: () => {},
};

Product.propTypes = {
  withPaginate: PropTypes.bool,
  hasMore: PropTypes.bool,
  loadFunc: PropTypes.func,
  addToCart: PropTypes.func.isRequired,
  products: PropTypes.instanceOf(Array).isRequired,
};

export default memo(Product);
