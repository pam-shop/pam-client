import React, { memo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import style from './style.scss';
import { saveProduct, unsaveProduct } from './action';

const like = '/save-ad.svg';
const unlike = '/save-ad-active.svg';

const STATE = {
  like: {
    icon: { src: like, alt: 'like', label: 'Lưu tin' },
    action: saveProduct,
  },
  unlike: {
    icon: { src: unlike, alt: 'unlike', label: 'Đã lưu' },
    action: unsaveProduct,
  },
};

const SaveProduct = ({ size, fullView, productId, customClassName, activeProducts, dispatch }) => {
  const state = activeProducts[productId] ? 'unlike' : 'like';
  const { icon, action } = STATE[state];

  const handleOnClick = () => {
    dispatch(action(productId));
  };

  const getItem = ({ label, alt, src }) =>
    ({
      true: () => (
        <button
          id="btn_save_ad"
          type="button"
          className={style.btnSavedProductFull}
          onClick={handleOnClick}
        >
          {label}
          &nbsp;
          <img width={size} src={src} alt={alt} />
        </button>
      ),
      false: () => (
        <button
          id="btn_save_ad"
          type="button"
          className={style.btnSavedProduct}
          onClick={handleOnClick}
        >
          <img width={size} src={src} alt={alt} />
        </button>
      ),
    }[fullView]);

  return <div className={`${style.saveProductWrapper} ${customClassName}`}>{getItem(icon)()}</div>;
};

SaveProduct.defaultProps = {
  fullView: false,
  size: 20,
  customClassName: '',
  activeProducts: {},
};

SaveProduct.propTypes = {
  size: PropTypes.number,
  fullView: PropTypes.bool,
  productId: PropTypes.number.isRequired,
  customClassName: PropTypes.string,
  activeProducts: PropTypes.instanceOf(Object),
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = ({ savedProduct }) => ({
  activeProducts: savedProduct.activeProducts,
});

export default connect(mapStateToProps, null)(memo(SaveProduct));
