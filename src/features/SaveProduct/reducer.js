import {
  LOAD_SAVED_PRODUCT,
  LOAD_SAVED_PRODUCT_SUCCESS,
  LOAD_SAVED_PRODUCT_FAIL,
  UNSAVE_PRODUCT_SUCCESS,
  SAVE_PRODUCT_SUCCESS,
  SAVE_PRODUCT_ACTION,
  UNSAVE_PRODUCT_ACTION,
  RESET_MESSAGE,
} from './action';

const INITIAL_STATE = {
  success: false,
  activeProducts: {},
  message: '',
  count: 0,
  data: [],
  limitAds: 50,
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case LOAD_SAVED_PRODUCT:
      return { ...state };

    case LOAD_SAVED_PRODUCT_SUCCESS: {
      const { products = [], limit } = action.result;
      const activeProducts = {};

      products.forEach((item) => {
        activeProducts[item.list_id] = true;
      });

      return {
        ...state,
        activeProducts,
        success: true,
        data: products,
        count: products.length,
        limitAds: limit,
      };
    }

    case LOAD_SAVED_PRODUCT_FAIL:
      return {
        ...state,
        activeProducts: {},
        success: false,
        error: action.error,
      };

    case SAVE_PRODUCT_SUCCESS: {
      const { status } = action.result;
      const data = SAVE_PRODUCT_ACTION[status]({ action, state });
      return {
        ...state,
        ...data,
      };
    }

    case UNSAVE_PRODUCT_SUCCESS: {
      const { status } = action.result;
      const data = UNSAVE_PRODUCT_ACTION[status]({ action, state });
      return {
        ...state,
        ...data,
      };
    }

    case RESET_MESSAGE: {
      return {
        ...state,
        message: '',
      };
    }

    default:
      return state;
  }
}
