export const LOAD_SAVED_PRODUCT = 'container/LOAD_SAVED_PRODUCT';
export const LOAD_SAVED_PRODUCT_SUCCESS = 'container/LOAD_SAVED_PRODUCT_SUCCESS';
export const LOAD_SAVED_PRODUCT_FAIL = 'container/LOAD_SAVED_PRODUCT_FAIL';

export const SAVE_PRODUCT = 'container/SAVE_PRODUCT';
export const SAVE_PRODUCT_SUCCESS = 'container/SAVE_PRODUCT_SUCCESS';
export const SAVE_PRODUCT_FAIL = 'container/SAVE_PRODUCT_FAIL ';

export const UNSAVE_PRODUCT = 'container/UNSAVE_PRODUCT';
export const UNSAVE_PRODUCT_SUCCESS = 'container/UNSAVE_PRODUCT_SUCCESS';
export const UNSAVE_PRODUCT_FAIL = 'container/UNSAVE_PRODUCT_FAIL ';

export const RESET_MESSAGE = 'container/RESET_MESSAGE ';

export function loadSaveProduct() {
  return {
    types: [LOAD_SAVED_PRODUCT, LOAD_SAVED_PRODUCT_SUCCESS, LOAD_SAVED_PRODUCT_FAIL],
    // promise: (client) => savedAdClient.with(client).loadSavedAd(),
  };
}

export function saveProduct(productId) {
  return {
    // types: [SAVE_PRODUCT, SAVE_PRODUCT_SUCCESS, SAVE_PRODUCT_FAIL],
    type: '',
    payload: {
      productId,
    },
    // promise: (client) => savedAdClient.with(client).saveAd(productId),
  };
}

export function unsaveProduct(productId) {
  return {
    type: '',
    // types: [UNSAVE_PRODUCT, UNSAVE_PRODUCT_SUCCESS, UNSAVE_PRODUCT_FAIL],
    payload: {
      productId,
    },
    // promise: (client) => savedAdClient.with(client).unsaveAd(productId),
  };
}

export const SAVE_PRODUCT_ACTION = {
  success: (res) => {
    const {
      state,
      action: {
        payload: { productId },
        result: { status, message },
      },
    } = res;
    const activeProducts = {
      ...state.activeProducts,
      [productId]: true,
    };

    return {
      success: status === 'success' ? true : false,
      count: state.count + 1,
      message,
      activeProducts,
    };
  },

  failed: (res) => {
    const {
      error: { message },
    } = res.action.error;
    return {
      success: false,
      message,
    };
  },
};

export const UNSAVE_PRODUCT_ACTION = {
  success: (res) => {
    const {
      state,
      action: {
        payload: { productId },
        result: { status, message },
      },
    } = res;
    const activeProducts = {
      ...state.activeProducts,
    };

    if (activeProducts[productId]) {
      delete activeProducts[productId];
    }

    return {
      success: status === 'success' ? true : false,
      count: state.count - 1,
      message,
      activeProducts,
    };
  },

  failed: (res) => {
    const {
      error: { message },
    } = res.action.error;
    return {
      success: false,
      message,
    };
  },
};

export function resetMessage() {
  return {
    type: RESET_MESSAGE,
  };
}
