import React, { memo } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';

const renderInfo = (infos) =>
  infos.map(({ label, value, className = '', styles = {}, onChangeQty, qty }) => (
    <div className={style.detail}>
      <div className={style.label}>{label}</div>
      {onChangeQty && (
        <input
          className={style.inputQty}
          max={100}
          min={1}
          type="number"
          onChange={onChangeQty}
          value={qty}
        />
      )}
      {value && (
        <div style={styles} className={`${style.value} ${className}`}>
          {value}
        </div>
      )}
    </div>
  ));

const ProductInfo = memo(
  ({
    productInfo: { priceString, title, product_code: productCode },
    onChangeQty,
    qty = 1,
    isMobile,
  }) => {
    const info = [
      {
        label: 'Mã số',
        value: productCode,
      },
      {
        label: 'Số lượng',
        onChangeQty,
        qty,
      },
      {
        label: 'Giá',
        value: priceString,
        styles: {
          'font-size': '24px',
        },
      },
    ];
    return (
      <div className={style.wrapper}>
        {!isMobile && <h1 className={style.title}>{title}</h1>}
        <div className={style.info}>{renderInfo(info)}</div>
      </div>
    );
  }
);

ProductInfo.propTypes = {
  productInfo: PropTypes.objectOf().isRequired,
  onChangeQty: PropTypes.func.isRequired,
  qty: PropTypes.number.isRequired,
  isMobile: PropTypes.bool.isRequired,
};

export default ProductInfo;
