import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { resetMessage } from '~/features/Cart/action';
import style from './style.scss';

const SnackBar = ({ message, autoHideDuration, dispatch }) => {
  const timerAutoHide = setTimeout(() => {
    dispatch(resetMessage());
  }, autoHideDuration);

  useEffect(
    () => () => {
      clearTimeout(timerAutoHide);
    },
    [message]
  );

  const hideMessage = () => {
    dispatch(resetMessage());
  };

  return (
    message && (
      <div className={style.snackBar}>
        {message}
        <img onClick={hideMessage} role="presentation" src="/close.png" alt="close" />
      </div>
    )
  );
};

SnackBar.defaultProps = {
  message: '',
  autoHideDuration: 3000,
};

SnackBar.propTypes = {
  message: PropTypes.string,
  autoHideDuration: PropTypes.number,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = ({ shoppingCart }) => ({
  message: shoppingCart.message,
});

export default connect(mapStateToProps, null)(memo(SnackBar));
