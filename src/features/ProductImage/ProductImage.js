import React, { memo, useState, useEffect } from 'react';
import dynamic from 'next/dynamic';
import PropTypes, { array } from 'prop-types';
import WrapperScroll from '~/components/WrapperScroll';
import style from './style.scss';

const PhotoSwipe = dynamic(() => import('react-photoswipe-2'), {
  ssr: true,
  loading: () => <div />,
});

const photoSwipeOptions = {
  closeOnScroll: false,
  shareEl: false,
  getDoubleTapZoom: (isMouseClick, item) => {
    if (isMouseClick) {
      return 1;
    }
    return item.initialZoomLevel < 0.7 ? 1 : 1.5;
  },
  history: false,
  preload: [1, 1],
  showHideOpacity: true,
  showAnimationDuration: 0,
  hideAnimationDuration: 0,
  zoomEl: false,
  clickToCloseNonZoomable: false,
  preloaderEl: true,
  arrowEl: true,
  captionEl: true,
  errorMsg: 'Không thể mở ảnh',
};

const ProductImage = memo(({ images, title, isMobile }) => {
  const [indexImage, setIndexImage] = useState(0);
  const [isOpenLB, setIsOpenLB] = useState(false);

  useEffect(() => {
    setIndexImage(0);
  }, [images]);

  const afterChange = (e) => {
    const index = e.getCurrentIndex();
    setIndexImage(index);
  };

  let photoSwipeItems = [];
  photoSwipeItems = images.map((url) => ({
    src: url.replace('http://', '//'),
    w: 1820,
    h: 1365,
  }));

  Object.assign(photoSwipeOptions, {
    index: indexImage,
  });

  const renderImages = () => {
    const htmlKeyword = [];
    images.forEach((item, index) => {
      const activeStyle = indexImage === index ? style.imagesActive : '';
      htmlKeyword.push(
        <div
          role="presentation"
          onClick={() => setIndexImage(index)}
          className={`${style.images} ${activeStyle}`}
          key={images[index]}
        >
          <img src={item} alt={title} />
        </div>
      );
    });
    return htmlKeyword;
  };

  const styleWrapper = isMobile ? style.wrapperImageMobile : style.wrapperImage;

  return (
    <div className={style.wrapper}>
      <div className={styleWrapper} role="presentation" onClick={() => setIsOpenLB(true)}>
        <img alt={title} src={images[indexImage]} />
      </div>
      <WrapperScroll className={style.wrapperScroll}>{renderImages()}</WrapperScroll>
      <PhotoSwipe
        isOpen={isOpenLB}
        items={photoSwipeItems}
        options={photoSwipeOptions}
        onClose={() => setIsOpenLB(false)}
        afterChange={afterChange}
      />
    </div>
  );
});

ProductImage.propTypes = {
  images: PropTypes.instanceOf(array).isRequired,
  title: PropTypes.string.isRequired,
  isMobile: PropTypes.bool.isRequired,
};

export default ProductImage;
