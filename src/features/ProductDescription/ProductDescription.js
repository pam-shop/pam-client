/* eslint-disable react/no-danger */
import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';

const htmlGuide = `<div id="tab_3" class="tab" style="display: block;"><p><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">Khi quý hàng mua hàng,&nbsp;<span style="color: rgb(0, 0, 255);"><strong>Kenta</strong></span>&nbsp;có chính sách dịch vụ để đảm bảo quyền lợi Quý Khách như sau:</span></span><br>
&nbsp;</p>

<p><span style="color: rgb(0, 0, 255);"><span style="font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">►</span></span><span style="color: rgb(255, 0, 0);"><span style="font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">&nbsp;</span></span><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><span style="font-size: 16px;"><span style="color: rgb(0, 0, 255);"><strong>Miễn phí giao hàng tận nơi - toàn quốc</strong></span></span><br>
&nbsp;&nbsp; &nbsp;Áp dụng đơn hàng trên 500k tại TP.HCM.<br>
&nbsp;&nbsp; &nbsp;Áp dụng đơn hàng trên 900k tại các tỉnh thành khác.<br>
&nbsp;&nbsp; &nbsp;</span></span><br>
<span style="color: rgb(0, 0, 255); font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">►</span><span style="font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">&nbsp;</span><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><span style="font-size: 16px;"><strong><span style="color: rgb(0, 0, 255);">Giao hàng tận nơi - Thu tiền tại chỗ trên Toàn Quốc</span></strong></span><br>
&nbsp;&nbsp; &nbsp;Thanh toán khi nhận hàng trên 63 tỉnh thành<br>
&nbsp;&nbsp; &nbsp;</span></span><br>
<span style="color: rgb(0, 0, 255); font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">►</span><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><span style="font-size: 16px;"><strong><span style="color: rgb(0, 0, 255);">&nbsp;Đổi hàng trong 5 ngày.</span></strong></span><br>
&nbsp;&nbsp; &nbsp;Sản phẩm chưa giặt, còn hóa đơn, tag.<br>
&nbsp;&nbsp; &nbsp;</span></span><br>
<span style="color: rgb(0, 0, 255); font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">►&nbsp;</span><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><span style="font-size: 16px;"><strong><span style="color: rgb(0, 0, 255);">Bảo hành sản phẩm</span></strong></span><br>
&nbsp;&nbsp; &nbsp;Với sản phẩm lỗi nút, dây kéo, sứt chỉ, lên line.<br>
&nbsp;&nbsp; &nbsp;</span></span><br>
<span style="color: rgb(0, 0, 255); font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">►&nbsp;</span><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><span style="font-size: 16px;"><strong><span style="color: rgb(0, 0, 255);">Khách hàng thân thiết</span></strong></span><br>
&nbsp;&nbsp; &nbsp;Tích điểm khi mua hàng để được giảm giá đến 15% và nhiều ưu đãi khác.<br>
&nbsp;&nbsp; &nbsp;</span></span><br>
<span style="color: rgb(0, 0, 255); font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">►</span><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><span style="font-size: 16px;"><strong><span style="color: rgb(0, 0, 255);">&nbsp;Gói quà miễn phí</span></strong></span><br>
&nbsp;&nbsp; &nbsp;Áp dụng cho mọi sản phẩm.<br>
&nbsp;&nbsp; &nbsp;</span></span><br>
<span style="color: rgb(0, 0, 255); font-family: Arial, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; widows: 3;">►</span><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><span style="font-size: 16px;"><strong><span style="color: rgb(0, 0, 255);">&nbsp;Cà thẻ miễn phí</span></strong></span><br>
&nbsp;&nbsp; &nbsp;Sử dụng thẻ ATM, Visa khi mua hàng.</span></span></p>

<p>&nbsp;</p>

<p><img alt="" src="/vnt_upload/guide/06_2014/giao_hang.png" style="width: 815px; height: 86px;"></p>

<p style="margin: 0px 0px 10px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; orphans: 3; widows: 3; line-height: 21px; font-family: Arial, sans-serif; text-align: center;"><strong style="font-family: verdana, geneva, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; text-align: center; widows: 3;">•&nbsp;</strong><span style="font-family: verdana, geneva, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; text-align: center; widows: 3;">Các đơn hàng có giá trị trên&nbsp;</span><strong style="font-family: verdana, geneva, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; text-align: center; widows: 3;">800k,&nbsp;</strong><span style="font-family: verdana, geneva, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; text-align: center; widows: 3;">quý khách vui lòng chuyển khoản trước cho</span><strong style="font-family: verdana, geneva, sans-serif; font-size: 14px; line-height: 21px; orphans: 3; text-align: center; widows: 3;">&nbsp;Kenta.</strong></p>

<p style="margin: 0px 0px 10px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; orphans: 3; widows: 3; line-height: 21px; font-family: Arial, sans-serif;">&nbsp;</p>

<p style="margin: 0px 0px 10px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; orphans: 3; widows: 3; line-height: 21px; font-family: Arial, sans-serif;">&nbsp;</p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(0, 0, 255);"><span style="font-size: 16px;"><u><strong>BẢO HÀNH</strong></u></span></span></span></p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><strong>Kenta có dịch vụ bảo hành, chỉnh sửa sản phẩm như sau</strong>:</span></span></p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">-Bảo hành 1 tháng với&nbsp;với các sản phẩm :&nbsp;<b>Áo khoác</b></span></span></p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><span style="font-size: 14px; font-family: verdana, geneva, sans-serif;">-Chỉnh sửa, lên line vừa vặn với số đo Khách hàng khi mua các sản phẩm:&nbsp;</span><strong style="font-size: 14px; font-family: verdana, geneva, sans-serif;">Quần tây, Quần Kaki, Quần jean</strong></p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">-Chỉnh sửa các sản phẩm bị lỗi nhẹ như:&nbsp;<strong>sứt chỉ, hư nút</strong></span></span></p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px;"><strong>-Sản phẩm bị trục trặc trong quá trình sử dụng, hoặc chưa hài lòng về chất lượng: quý khách có thể đem sản phẩm đến trực tiếp tại cửa hàng, hoặc phản hồi vào kentasale@gmail.com để được xử lý nhanh nhất.</strong></span></span></p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;">&nbsp;</p>

<p style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><span style="color:#0000FF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 16px;"><u><b>ĐỔI HÀNG</b></u></span></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="color: rgb(0, 0, 255);"><span style="font-size: 14px;"><strong style="line-height: 1.6em;">*Quy cách sản phẩm:</strong></span></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">Còn nguyên nhãn mác (đối với hàng chưa qua sử dụng).</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">Hàng còn mới, không bị làm dơ bẩn, hư hỏng bởi những tác nhân bên ngoài cửa hàng sau khi mua hàng.</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="color: rgb(0, 0, 255);"><span style="font-size: 14px;"><strong>*Các mặt hàng không nhận đổi hàng (đối với hàng chưa qua sử dụng):</strong></span></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">Thời gian mua hàng quá&nbsp;<strong>05 ngày.</strong></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">Không có hóa đơn mua hàng.</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">Nhãn mác không còn nguyên vẹn, các phụ kiện của hàng không còn đầy đủ hoặc bị hư hại do việc sử dụng không theo hướng dẫn trên sản phẩm.</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">Hàng giảm giá, hàng phụ kiện.</span></span></p>

<p style="line-height: 20.7999992370605px;">&nbsp;</p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="color: rgb(0, 0, 255);"><span style="font-size: 14px;"><strong><u>1/ Khách hàng đến mua hàng trực tiếp tại Kenta:</u></strong></span></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">-Điều kiện thời gian để Khách hàng đổi hàng:&nbsp;<strong>1 lần trong vòng 05 ngày kể từ ngày mua hàng</strong>. Hàng đổi là sản phẩm còn mới chưa qua sử dụng và phải có&nbsp;<strong>HÓA ĐƠN MUA HÀNG.</strong></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><strong>-Không áp dụng hình thức trả hàng để hoàn tiền</strong><strong>.</strong></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><strong>-Khách hàng có thể đổi lấy 1 sản phẩm khác có giá trị tương đương hoặc cao hơn.</strong></span></span></p>

<p style="line-height: 20.7999992370605px;">&nbsp;</p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="color: rgb(0, 0, 255);"><span style="font-size: 14px;"><strong><u>2/ Khách hàng mua hàng Online:</u></strong></span></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">- Khách hàng được đổi hàng khi hàng không đúng mã sản phẩm đã đặt, đổi size trong vòng<strong>&nbsp;5 ngày ko tính thời gian vận chuyển.</strong></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">- Hàng đổi trả là hàng phải còn nguyên vẹn nhãn mác, tem giá, bao bì đóng gói.<br>
<br>
-&nbsp;Quý khách ở TP.HCM vui lòng đến đổi sản phẩm&nbsp;trực tiếp tại cửa hàng, đ</span></span><span style="font-family: verdana, geneva, sans-serif; font-size: 14px; line-height: 20.7999992370605px;">ể thuận tiện cho việc đổi hàng và lựa chọn sản phẩm mới.</span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">- Nếu trong trường hợp sản phẩm quý khách muốn đổi đã hết hàng. Quý khách vui lòng lựa chọn mẫu khác.&nbsp;Kenta không áp dụng hình thức hoàn tiền bao gồm&nbsp;phí vận chuyển.</span></span></p>

<p style="line-height: 20.7999992370605px;">&nbsp;</p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="color: rgb(0, 0, 255);"><span style="font-size: 14px;"><strong>Cách thức đổi trả hàng (Khách hàng đặt hàng qua mạng):</strong></span></span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><strong>Bước 1</strong>: Khách hàng liên hệ đến KENTA qua số điện thoại&nbsp;<strong>(08) 66835051</strong>&nbsp;để được nhân viên tiếp nhận thông tin đồi trả hàng.</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><strong>Bước 2</strong>: Khách hàng gửi trả lại sản phẩm cho KENTA theo địa chỉ:</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">KENTA.VN - 20 Cửu Long, P15, Q10, TPHCM</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;">SĐT<strong>:</strong>&nbsp;(08) 66835051</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><strong>Bước 3:</strong>&nbsp;Sau khi nhận lại hàng, kiểm tra và chấp nhận sản phẩm mà khách hàng trả lại, Nhân viên KENTA sẽ liên lạc với quý khách để đổi trả hàng.</span></span></p>

<p style="line-height: 20.7999992370605px;"><span style="font-family: verdana, geneva, sans-serif;"><span style="font-size: 14px;"><strong>* Thời gian đổi hàng: từ 14h hàng ngày.</strong></span></span></p>

<div>&nbsp;</div>

<p style="margin: 0px 0px 10px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; orphans: 3; widows: 3; line-height: 21px; font-family: Arial, sans-serif;">&nbsp;</p>

<p style="margin: 0px 0px 10px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; orphans: 3; widows: 3; line-height: 21px; font-family: Arial, sans-serif;">&nbsp;</p>

<p style="margin: 0px 0px 10px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; orphans: 3; widows: 3; line-height: 21px; font-family: Arial, sans-serif;"><span style="font-family: verdana, geneva, sans-serif;">Mọi&nbsp;ý kiến về &nbsp;dịch vụ&nbsp;quý khách&nbsp;vui lòng liên hệ:</span></p>

<address style="margin: 0px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; font-family: Arial, sans-serif; line-height: 21px;"><span style="font-family: verdana, geneva, sans-serif;">Tel:&nbsp;<em style="margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;">(08).66835051 &nbsp;- 0908483900</em></span></address>

<address style="margin: 0px; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; font-family: Arial, sans-serif; line-height: 21px;"><span style="font-family: verdana, geneva, sans-serif;">Email: kentasale@gmail.com</span></address></div>`;

const renderInfo = ({ name = '', onClick, activeTab, type }) => (
  <div
    onClick={onClick}
    className={`${style.elementInfo} ${activeTab === type ? style.elementInfoActive : ''}`}
    tabIndex={0}
    role="button"
  >
    <span>{name}</span>
  </div>
);

renderInfo.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  activeTab: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

const menuSortByType = [
  {
    name: 'Mô tả',
    type: 0,
  },
  {
    name: 'Hướng dẫn mua hàng',
    type: 1,
  },
  {
    name: 'Bảo hành',
    type: 2,
  },
];

const ProductDescription = memo(({ description }) => {
  const [activeTab, setActiveTab] = useState(0);

  const renderActiveTab = (active) =>
    ({
      0: <div dangerouslySetInnerHTML={{ __html: description }} />,
      1: <div dangerouslySetInnerHTML={{ __html: htmlGuide }} />,
      2: '2',
    }[active]);

  return (
    <>
      <div className={style.wrapper}>
        <div className={style.infoAds}>
          {menuSortByType.map((item, index) =>
            renderInfo({
              name: item.name,
              type: item.type,
              activeTab,
              onClick: () => setActiveTab(index),
            })
          )}
        </div>
      </div>
      <div className={style.main}>{renderActiveTab(activeTab)}</div>
    </>
  );
});

ProductDescription.propTypes = {
  description: PropTypes.string.isRequired,
};

export default ProductDescription;
