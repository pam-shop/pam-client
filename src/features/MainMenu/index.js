import React, { memo, useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import style from './style.scss';
import routes from '~/routes';
import { insertDivideToArray, isActiveRouter } from '~/helpers/utils';
import Caret from '~/components/Caret';
import fakeCategories from '~/fakeData/category';
import Cart from '~/features/Cart';
import Account from '~/features/Account';

const isProductView = (pathname) => pathname === '/productView';

const { Link } = routes;

const MainMenu = ({ isMobile, categories }) => {
  const [fixed, setFixed] = useState(false);
  const [showDropDown, setShowDropDown] = useState({});
  const router = useRouter();
  const menuRef = useRef(null);

  const handleScroll = () => {
    const { current = null } = menuRef;
    const offset = current.offsetTop + current.clientHeight;
    if (window.pageYOffset > offset) {
      setFixed(true);
    } else {
      setFixed(false);
    }
  };

  useEffect(() => {
    if (!isMobile) {
      window.addEventListener('scroll', handleScroll);
    }

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  if (isMobile) {
    return null;
  }

  const handleMouseEnter = ({ category, subCategory = [] }) => {
    if (subCategory.length > 0) {
      setShowDropDown({ [category]: true });
    }
  };

  const handleMouseLeave = () => {
    setShowDropDown({});
  };

  const fixedStyle = fixed ? `${style.mainMenu} ${style.fixed}` : style.mainMenu;
  const checkProductView = isProductView(router.pathname);

  return (
    <div className={fixedStyle} ref={menuRef}>
      <ul>
        {categories.map((cate) => {
          const showSubMenu =
            cate?.subCategory?.length > 0 ? insertDivideToArray(cate.subCategory) : null;
          const activeStyle =
            isActiveRouter(router, cate.href) && !checkProductView
              ? {
                  color: '#ffba00',
                  fontWeight: 'bold',
                }
              : {};
          return (
            <li
              key={cate.category}
              onMouseLeave={handleMouseLeave}
              onMouseEnter={() => handleMouseEnter(cate)}
            >
              <Link route={`/${cate.href}`} href={cate.href}>
                <a className={style.mainMenuLink} style={activeStyle}>
                  <span>{cate.name}</span> {showSubMenu && <Caret />}
                </a>
              </Link>
              {showDropDown[cate.category] && (
                <div className={style.mainMenuSubcategory}>
                  {showSubMenu.map((subCate) => {
                    const activeSubStyle =
                      isActiveRouter(router, subCate.href) && !checkProductView
                        ? {
                            color: '#ffba00',
                            fontWeight: 'bold',
                          }
                        : {};
                    if (!subCate.devider) {
                      return (
                        <Link route={`/${cate.href}/${subCate.href}`} key={subCate.subCategory}>
                          <a className={style.mainMenuLink} style={activeSubStyle}>
                            <span>{subCate.name}</span>
                          </a>
                        </Link>
                      );
                    }
                    return <div key={`devider${subCate.key}`} className={style.devider} />;
                  })}
                </div>
              )}
            </li>
          );
        })}
        {fixed && (
          <>
            <li>
              <Account isFixed />
            </li>
            <li>
              <Cart isFixed />
            </li>
          </>
        )}
      </ul>
    </div>
  );
};

MainMenu.defaultProps = {
  categories: fakeCategories,
};

MainMenu.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  categories: PropTypes.instanceOf(Array),
};

export default memo(MainMenu);
