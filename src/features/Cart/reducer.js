import {
  ADD_PRODUCT_TO_CART,
  UPDATE_PRODUCT_TO_CART,
  DELETE_PRODUCT_TO_CART,
  RESET_MESSAGE,
  ADD_PROMOTION_TO_CART,
  LOAD_INIT_CART,
} from './action';
import { helperAddToCart, helperQtyToCart, getAmountCart, getNewState } from './helpers';

const initialState = {
  cart: [],
  currentAmount: 0,
  totalAmount: 0,
  promotion: null,
  promotionAmount: 0,
  user: null,
  message: '',
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_INIT_CART: {
      const obj = JSON.parse(localStorage.getItem('cart')) || [];
      return {
        ...state,
        ...obj,
      };
    }

    case ADD_PROMOTION_TO_CART: {
      const tmpObj = {
        ...state,
        promotion: action.promotion,
      };
      const objCart = getAmountCart(tmpObj);
      return {
        ...state,
        ...objCart,
      };
    }

    case ADD_PRODUCT_TO_CART: {
      const cart = helperAddToCart(state.cart, action.product, action.qty);
      const objCart = getAmountCart({ ...state, cart });
      const newState = getNewState({
        ...state,
        cart,
        ...objCart,
      });
      return {
        ...newState,
        message: 'Thêm giỏ hàng thành công',
      };
    }

    case UPDATE_PRODUCT_TO_CART: {
      const cart = helperQtyToCart(state.cart, action.productId, action.qty);
      const objCart = getAmountCart({ ...state, cart });
      const newState = getNewState({
        ...state,
        cart,
        ...objCart,
      });
      return newState;
    }

    case DELETE_PRODUCT_TO_CART: {
      const newCart = state.cart.filter((pdt) => pdt.product.productId !== action.productId);
      const objCart = getAmountCart({ ...state, cart: newCart });
      const newState = getNewState({
        ...state,
        cart: newCart,
        ...objCart,
      });
      return { ...newState, message: 'Xóa giỏ hàng thành công.' };
    }

    case RESET_MESSAGE: {
      return { ...state, message: '' };
    }

    default:
      return state;
  }
}
