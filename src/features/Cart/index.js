import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import routes from '~/routes';
import style from './style.scss';
import { PATTERN_URL } from '~/constants/routes';

const { Link } = routes;

const Cart = ({ isFixed, shoppingCart }) => {
  const { cart } = shoppingCart;
  const styles = {
    marginTop: isFixed ? '0px' : '',
  };
  return (
    <div className={style.Cart} style={styles}>
      <Link route={PATTERN_URL.SHOPPING_CART}>
        <a>
          <img src="/icon_giohang.png" alt="Giỏ hàng" />
          {cart.length > 0 && <span className={style.CartCount}>{cart.length}</span>}
        </a>
      </Link>
    </div>
  );
};

Cart.defaultProps = {
  isFixed: false,
};

Cart.propTypes = {
  isFixed: PropTypes.bool,
  shoppingCart: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = ({ shoppingCart }) => ({
  shoppingCart,
});

export default connect(mapStateToProps, null)(memo(Cart));
