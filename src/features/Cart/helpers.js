export const convertPriceToFloat = (price) =>
  Number(price.includes('.') ? price.split(',').join('') : price);

export const converntNumberToPrice = (num) => {
  if (!num) {
    return '0 đ';
  }
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
};

export const helperAddToCart = (cart, product, qty = null) => {
  const iP = cart.findIndex(({ product: { productId } }) => productId === product.productId);
  if (iP === -1) {
    const objProduct = {
      product,
      qty: qty || 1,
    };
    cart.push(objProduct);
  } else {
    cart[iP]['qty'] = qty ? qty + cart[iP]['qty'] : cart[iP]['qty']++;
  }

  return cart;
};

export const getTotalAmount = (cart) => {
  let result = 0;
  cart.forEach((item, i) => {
    const price = parseInt(cart[i].product.price, 10);
    const qty = parseInt(cart[i].qty, 10);
    result += price * qty;
  });
  return parseInt(result.toString(), 10);
};

export const getAmountCart = ({ cart, promotionAmount, promotion, totalAmount }) => {
  const currentAmount = getTotalAmount(cart);
  if (promotion === 0 || !promotion) {
    totalAmount = currentAmount;
  } else {
    totalAmount = currentAmount - (currentAmount * promotion) / 100;
    promotionAmount = (currentAmount * promotion) / 100;
  }

  const tmpcart = {
    currentAmount,
    totalAmount,
    promotion,
    promotionAmount,
    currentAmountString: converntNumberToPrice(currentAmount),
    totalAmountString: converntNumberToPrice(totalAmount),
    promotionAmountString: converntNumberToPrice(promotionAmount),
  };

  return tmpcart;
};

export const helperQtyToCart = (cart, productId, qty) => {
  const iP = cart.findIndex(({ product }) => product.productId === productId);
  if (iP !== -1) {
    cart[iP]['qty'] = qty;
  }
  return cart;
};

export const getNewState = (state) => {
  if (state?.message) {
    state['message'] = '';
  }
  localStorage.setItem('cart', JSON.stringify(state));
  return state;
};
