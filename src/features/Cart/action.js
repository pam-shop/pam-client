export const ADD_PRODUCT_TO_CART = 'cart/ADD_PRODUCT_TO_CART';
export const UPDATE_PRODUCT_TO_CART = 'cart/UPDATE_PRODUCT_TO_CART';
export const DELETE_PRODUCT_TO_CART = 'cart/DELETE_PRODUCT_TO_CART';
export const ADD_PROMOTION_TO_CART = 'cart/ADD_PROMOTION_TO_CART';
export const PAYMENT_TO_CART = 'cart/PAYMENT_TO_CART';

export const RESET_MESSAGE = 'container/RESET_MESSAGE';
export const LOAD_INIT_CART = 'container/LOAD_INIT_CART';

export const resetMessage = () => ({
  type: RESET_MESSAGE,
});

export const loadInitCart = () => ({
  type: LOAD_INIT_CART,
});

export const addToCart = (product, qty = null) => ({
  type: ADD_PRODUCT_TO_CART,
  product,
  qty,
});

export const updateToCart = (productId, qty) => ({
  type: UPDATE_PRODUCT_TO_CART,
  productId,
  qty,
});

export const deleteToCart = (productId) => ({
  type: DELETE_PRODUCT_TO_CART,
  productId,
});

export const addPromotionToCart = (promotion) => ({
  type: ADD_PROMOTION_TO_CART,
  promotion,
});

export const donePaymentToCart = () => ({
  type: PAYMENT_TO_CART,
});
