import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import Range from 'rc-slider/lib/Range';
import style from './style.scss';
import { formatNumberValue } from '~/helpers/utils';
import { setFilterPrice } from './action';

const FilterPrice = ({ priceMax, interval, filterPrice }) => {
  const dispatch = useDispatch();

  const handleRanger = (newValue) => {
    const priceValue = { min: parseInt(newValue[0], 10), max: parseInt(newValue[1], 10) };
    dispatch(setFilterPrice(priceValue));
  };

  const maxValue =
    priceMax < filterPrice.max
      ? formatNumberValue(priceMax) + '+'
      : formatNumberValue(filterPrice.max);

  return (
    <div className={style.rangerWrapper}>
      <div className={style.priceFromTo}>
        {filterPrice.max !== filterPrice.min ? (
          <div>
            Giá từ <b>{formatNumberValue(filterPrice.min)}</b> đến <b>{maxValue}</b>
          </div>
        ) : (
          <div>
            Giá bằng <b>{maxValue}</b>
          </div>
        )}
      </div>
      <div className={style.Range}>
        <Range
          allowCross={false}
          value={[filterPrice.min, filterPrice.max]}
          onChange={handleRanger}
          step={priceMax / interval}
          min={0}
          max={parseInt(priceMax, 10) + priceMax / interval}
        />
      </div>
    </div>
  );
};

FilterPrice.defaultProps = {
  interval: 20000,
};

FilterPrice.propTypes = {
  priceMax: PropTypes.number.isRequired,
  interval: PropTypes.number,
  filterPrice: PropTypes.instanceOf(Object).isRequired,
};

export default memo(FilterPrice);
