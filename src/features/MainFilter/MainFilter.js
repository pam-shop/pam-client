import React, { memo } from 'react';
import { connect, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import style from './style.scss';
import FilterPrice from './FilterPrice';
import { pushRouter } from '~/helpers/router';
import { queryBuilder, getQueryFilter } from '~/helpers/utils';
import { resetFilter } from './action';

const MainFilter = ({ mainFilter: { filterPrice }, query, pathname }) => {
  const dispatch = useDispatch();

  const handleApplyFilter = () => {
    const newQuery = {
      ...query,
      price: `${filterPrice.min}-${filterPrice.max}`,
    };
    const href = `${pathname}${queryBuilder(newQuery)}`;
    pushRouter(href);
  };

  const handleResetFilter = () => {
    dispatch(resetFilter());
    pushRouter(pathname);
  };

  const isDisable = getQueryFilter(query);

  return (
    <div className={style.wrapperFilter}>
      <div className={style.wrapper}>
        <FilterPrice priceMax={1000000} filterPrice={filterPrice} />
      </div>
      <div className={style.wrapperButton}>
        <button
          type="button"
          onClick={handleApplyFilter}
          className={`btn-main ${style.buttonApply}`}
        >
          Lọc
        </button>
        {!isDisable && (
          <button
            type="button"
            onClick={handleResetFilter}
            className={`btn-main ${style.buttonApply}`}
          >
            Đặt lại
          </button>
        )}
      </div>
    </div>
  );
};

MainFilter.propTypes = {
  mainFilter: PropTypes.instanceOf(Object).isRequired,
  query: PropTypes.instanceOf(Object).isRequired,
  pathname: PropTypes.string.isRequired,
};

const mapStateToProps = ({ mainFilter }) => ({
  mainFilter,
});

export default connect(mapStateToProps, null)(memo(MainFilter));
