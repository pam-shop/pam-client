import { FILTER_PRICE, RESET_FILTER } from './action';

const initialState = {
  filterPrice: {
    min: 0,
    max: 10000000,
  },
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FILTER_PRICE:
      return { ...state, filterPrice: action.price };

    case RESET_FILTER: {
      return initialState;
    }

    default:
      return state;
  }
};
