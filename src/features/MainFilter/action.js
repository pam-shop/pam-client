export const FILTER_PRICE = 'filter/FILTER_PRICE';
export const RESET_FILTER = 'filter/RESET_FILTER';

export const setFilterPrice = (value) => ({
  type: FILTER_PRICE,
  price: value,
});

export const resetFilter = () => ({
  type: RESET_FILTER,
});
