import React, { memo, useRef, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AutoAffix from 'react-overlays/lib/AutoAffix';
import style from './style.scss';
import CartItem from './CartItem';
import fakeDiscount from '~/fakeData/discount';
import Label from '~/components/Label';
import { pushRouter } from '~/helpers/router';
import { PATTERN_URL } from '~/constants/routes';
import { addPromotionToCart } from '~/features/Cart/action';

const MyCart = ({ shoppingCart, dispatch }) => {
  const { cart, promotion, currentAmountString, totalAmountString } = shoppingCart;
  const [valuePromotion, setValuePromotion] = useState(promotion);
  const [errorPromotion, setErrorPromotion] = useState(false);
  const refCart = useRef(null);

  const handleOnClickPayment = () => {
    pushRouter(PATTERN_URL.PAYMENT);
  };

  const handleOnChangeDiscount = (e) => {
    const { value } = e.target;
    setValuePromotion(value);
    setErrorPromotion(false);
  };

  const handleOnSubmitDiscount = (e) => {
    e.preventDefault();
    const indexPromo = fakeDiscount.findIndex((pro) => pro.code === valuePromotion);
    if (indexPromo !== -1) {
      dispatch(addPromotionToCart(fakeDiscount[indexPromo].value));
    } else {
      setErrorPromotion(true);
    }
  };

  return (
    <div className={style.myCart}>
      {errorPromotion && (
        <Label
          text={`Mã giảm giá "${valuePromotion}" không hợp lệ (chương trình khuyến mãi tương ứng không tồn tại)`}
          type="error"
        />
      )}
      <h2 className={style.titlePage}>
        Giỏ hàng <span>({cart.length} sản phẩm )</span>
      </h2>
      <div className="col-md-9 no-padding" ref={refCart}>
        <div className={style.shoppingCart}>
          {cart.map(({ product, qty }) => (
            <CartItem product={product} qty={qty} dispatch={dispatch} />
          ))}
        </div>
      </div>
      <div className="col-md-3" style={{ paddingRight: '0' }}>
        <AutoAffix viewportOffsetTop={60} container={refCart?.current}>
          <div>
            <div className={style.wrapper}>
              <div className={style.wrapperFee}>
                <p className={style.wrapperFeeText}>
                  <span>Tạm tính: </span>
                  <strong>{currentAmountString}</strong>
                </p>
              </div>
              {promotion && (
                <div className={style.wrapperFee}>
                  <p className={style.wrapperFeeText}>
                    <span>Giảm giá: </span>
                    <strong className={style.wrapperFeeTextDiscount}>-{promotion}%</strong>
                  </p>
                </div>
              )}
              <div className={style.wrapperFee}>
                <p className={style.wrapperFeeText}>
                  <span>Tổng: </span>
                  <strong className={style.amount}>{totalAmountString}</strong>
                </p>
              </div>
              <button
                onClick={handleOnClickPayment}
                type="button"
                tabIndex={0}
                className={`btn-main ${style.buttonPayment}`}
              >
                Thanh toán
              </button>
              <br />
              <div className={style.wrapperDiscount}>
                <p className={style.wrapperFeeText}>
                  <span>Mã giảm giá / quà tặng </span>
                </p>
                <form onSubmit={handleOnSubmitDiscount}>
                  <input onChange={handleOnChangeDiscount} />
                  <button
                    className="btn-secondary"
                    onClick={handleOnSubmitDiscount}
                    tabIndex={0}
                    type="button"
                  >
                    Nhập
                  </button>
                </form>
              </div>
            </div>
          </div>
        </AutoAffix>
      </div>
    </div>
  );
};

MyCart.propTypes = {
  shoppingCart: PropTypes.instanceOf(Object).isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect(null, null)(memo(MyCart));
