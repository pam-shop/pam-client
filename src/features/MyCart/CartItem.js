import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';
import routes from '~/routes';
import { updateToCart, deleteToCart } from '~/features/Cart/action';
import { buildProductUrl } from '~/helpers/utils';

const { Link } = routes;

const handleErrorImage = (e) => {
  const image = e.target;
  image.style.display = 'none';
};

const CartItem = ({ product, qty, dispatch }) => {
  const [qtyP, setQtyP] = useState(qty);
  const productUrl = buildProductUrl(product);

  const handleOnDelete = () => {
    dispatch(deleteToCart(product.productId));
  };

  const handleOnChange = (e) => {
    const { value, max } = e.target;
    if (value > -1 && value < max) {
      setQtyP(value);
    } else {
      setQtyP(0);
    }
  };

  const handleQty = (isIncrease) => {
    let tempQty = qtyP;
    if (isIncrease && tempQty + 1 <= product.qty) {
      tempQty += 1;
    }

    if (!isIncrease && tempQty - 1 >= 0) {
      tempQty -= 1;
    }

    if (!isIncrease && tempQty === 0) {
      dispatch(deleteToCart(product.productId));
    }

    if (tempQty !== qtyP) {
      setQtyP(tempQty);
      dispatch(updateToCart(product.productId, tempQty));
    }
  };

  const styleHasSale = product.isSale
    ? {
        color: 'grey',
        fontSize: '13px',
        fontWeight: '500',
        textDecoration: 'line-through',
      }
    : {};

  return (
    <div className={style.shoppingCartItem}>
      <div className={style.description}>
        <div className={style.thumbnail}>
          <img src={product.thumbnail} alt={product.title} onError={handleErrorImage} />
        </div>
        <div className={style.main}>
          <Link route={productUrl}>
            <a className={style.title}>{product.title}</a>
          </Link>
          <div className={style.wrapperPrice}>
            {product.isSale && product.priceSale && (
              <span className={style.priceSale}>
                {product.priceSaleString}
                {product.discount && <span className={style.discount}>-{product.discount}</span>}
              </span>
            )}
            <span style={styleHasSale} className={style.price}>
              {product.priceString} đ
            </span>
          </div>
          <div className={style.imageTag}>
            {product.isSale && (
              <div className={style.saleTag}>
                <img src="/sale.png" alt="sale" />
              </div>
            )}
            {product.isFreeShipping && (
              <div className={style.freeShpping}>
                <img src="/free-shipping.png" alt="free-shipping" />
              </div>
            )}
          </div>
        </div>
      </div>
      <div className={style.wrapperDelete} tabIndex={0} role="button" onClick={handleOnDelete}>
        <img src="/delete.jpg" alt="delete" />
      </div>
      <div className={style.wrapperQty}>
        <span className={style.inputGroup}>
          <button
            className={`btn btn-default ${style.buttonDecrease}`}
            onClick={() => handleQty()}
            type="button"
          >
            -
          </button>
        </span>
        <input type="number" onChange={handleOnChange} min={1} max={product.qty} value={qtyP} />
        <span className={style.inputGroup}>
          <button
            className={`btn btn-default ${style.buttonIncrease}`}
            onClick={() => handleQty(true)}
            type="button"
          >
            +
          </button>
        </span>
      </div>
    </div>
  );
};

CartItem.propTypes = {
  product: PropTypes.instanceOf(Object).isRequired,
  qty: PropTypes.number.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default memo(CartItem);
