// @ts-nocheck
import NextRoutes from 'next-routes';
import { ROUTE_NAME, PATTERN_URL, PAGE_NAME } from './constants/routes';

const routes = new NextRoutes();

// http://localhost:8080/chi-tiet-san-pham/32302
routes.add({
  name: ROUTE_NAME.PRODUCT_VIEW,
  pattern: PATTERN_URL.PRODUCT_VIEW,
  page: PAGE_NAME.PRODUCT_VIEW,
});

// http://localhost:8080/huong-dan-mua-hang
routes.add({
  name: ROUTE_NAME.SHOPPING_GUIDE,
  pattern: PATTERN_URL.SHOPPING_GUIDE,
  page: PAGE_NAME.SHOPPING_GUIDE,
});

// http://localhost:8080/lien-he
routes.add({
  name: ROUTE_NAME.CONTACT,
  pattern: PATTERN_URL.CONTACT,
  page: PAGE_NAME.CONTACT,
});

// http://localhost:8080/gioi-thieu
routes.add({
  name: ROUTE_NAME.ABOUT,
  pattern: PATTERN_URL.ABOUT,
  page: PAGE_NAME.ABOUT,
});

// http://localhost:8080/gio-hang
routes.add({
  name: ROUTE_NAME.SHOPPING_CART,
  pattern: PATTERN_URL.SHOPPING_CART,
  page: PAGE_NAME.SHOPPING_CART,
});

// http://localhost:8080/thanh-toan
routes.add({
  name: ROUTE_NAME.PAYMENT,
  pattern: PATTERN_URL.PAYMENT,
  page: PAGE_NAME.PAYMENT,
});

// http://localhost:8080/trang-chu
routes.add({
  name: ROUTE_NAME.HOME_FULL,
  pattern: PATTERN_URL.HOME_FULL,
  page: PAGE_NAME.HOME,
});

// http://localhost:8080/ao-khoac
routes.add({
  name: ROUTE_NAME.CATEGORY,
  pattern: PATTERN_URL.CATEGORY,
  page: PAGE_NAME.CATEGORY,
});

// http://localhost:8080/ao-khoac/ao-khoac-du/20323-SM-3909
routes.add({
  name: ROUTE_NAME.PRODUCT_VIEW_FULL,
  pattern: PATTERN_URL.PRODUCT_VIEW_FULL,
  page: PAGE_NAME.PRODUCT_VIEW,
});

// http://localhost:8080/ao-khoac/ao-khoac-du
routes.add({
  name: ROUTE_NAME.SUB_CATEGORY,
  pattern: PATTERN_URL.SUB_CATEGORY,
  page: PAGE_NAME.CATEGORY,
});

// http://localhost:8080
routes.add({
  name: ROUTE_NAME.HOME,
  pattern: PATTERN_URL.HOME,
  page: PAGE_NAME.HOME,
});

export default routes;
