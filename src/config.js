const pkg = require('../package.json');

const assetPrefix =
  pkg && pkg.version && process.env.ASSET_DOMAIN
    ? `${process.env.ASSET_DOMAIN}/${pkg.version}`
    : '';

module.exports = {
  app: {
    title: 'PAM SHOP - Website Mua Bán Trực Tuyến Hàng Đầu Của Người Việt - PAM SHOP',
    head: {
      meta: [
        {
          key: 'meta_charSet',
          charSet: 'utf-8',
        },
        {
          key: 'meta_name',
          name: 'name',
          content: 'PAM SHOP - Website Mua Bán, Rao Vặt Trực Tuyến Hàng Đầu Của Người Việt',
        },
        {
          key: 'meta_description',
          name: 'description',
          content:
            'PAM SHOP - Website mua bán rao vặt của người Việt với hàng ngàn món hời đang được rao bán mỗi ngày. Đăng tin mua bán UY TÍN, NHANH CHÓNG, AN TOÀN.',
        },
        {
          key: 'meta_og_title',
          property: 'og:title',
          content: 'PAM SHOP - Website Mua Bán, Rao Vặt Trực Tuyến Hàng Đầu Của Người Việt',
        },
        {
          key: 'meta_og_description',
          property: 'og:description',
          content:
            'PAM SHOP - Website mua bán rao vặt của người Việt với hàng ngàn món hời đang được rao bán mỗi ngày. Đăng tin mua bán UY TÍN, NHANH CHÓNG, AN TOÀN.',
        },
        {
          key: 'meta_og_type',
          property: 'og:type',
          content: 'website',
        },
        {
          key: 'meta_viewport',
          name: 'viewport',
          content:
            'width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0',
        },
        {
          key: 'meta_mobile_web_app_capable',
          name: 'mobile-web-app-capable',
          content: 'yes',
        },
        {
          key: 'meta_apple_mobile_web_app_capable',
          name: 'apple-mobile-web-app-capable',
          content: 'yes',
        },
        {
          key: 'meta_application_name',
          name: 'application-name',
          content: 'pamshop',
        },
        {
          key: 'meta_apple_mobile_web_app_status_bar_style',
          name: 'apple-mobile-web-app-status-bar-style',
          content: 'black',
        },
        {
          key: 'meta_apple_mobile_web_app_title',
          name: 'apple-mobile-web-app-title',
          content: 'pamshop.com',
        },
        {
          key: 'meta_theme_color',
          name: 'theme-color',
          content: '#FDCE09',
        },
      ],
    },
  },
  assetPrefix,
  env: (typeof window !== 'undefined' ? window.ENV : process.env.NODE_ENV) || 'development',
  fbId: '396474964155316',
};
