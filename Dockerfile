FROM node:12.16.1

RUN mkdir -p /pam-client
WORKDIR /pam-client

COPY package.json /pam-client/
COPY index.d.ts /pam-client/
COPY yarn.lock /pam-client/

RUN yarn install
COPY . /pam-client/
RUN yarn build

RUN apk add --no-cache tzdata && \
  cp -f /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime && \
  echo "Asia/Ho_Chi_Minh" > /etc/timezone

EXPOSE 8080
ENTRYPOINT ["yarn"]
CMD ["start"]
