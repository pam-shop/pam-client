/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars, global-require */
const firebase = require('firebase');
const path = require('path');
const fs = require('fs');
const XMLHttpRequest = require('xhr2');

global.XMLHttpRequest = XMLHttpRequest;

const firebaseConfig = {
  apiKey: 'AIzaSyDPvHG2Lx4Ajy7LQFI49PijBN8um_O19Qc',
  authDomain: 'tess-fa402.firebaseapp.com',
  databaseURL: 'https://tess-fa402.firebaseio.com',
  projectId: 'tess-fa402',
  storageBucket: 'tess-fa402.appspot.com',
  messagingSenderId: '208440894144',
  appId: '1:208440894144:web:c6eb891b094dad7e08c325',
};
require('firebase/storage');

firebase.initializeApp(firebaseConfig);

const storageRef = firebase.storage().ref('/images');

const directoryPath = path.resolve(__dirname, '../../public/');

function upload() {
  const fileList = [];

  function uploadDirectory() {
    let dirCtr = 1;
    let itemCtr = 0;

    function getFiles(directory) {
      fs.readdir(directory, (err, items) => {
        dirCtr--;
        itemCtr = items.length;
        items.forEach((item) => {
          const fullPath = path.join(directory, item);

          fs.stat(fullPath, (statErr, stat) => {
            itemCtr--;
            const file = fs.readFileSync(fullPath);
            if (stat.isFile()) {
              fileList.push({
                file,
                name: item,
              });
            } else if (stat.isDirectory()) {
              dirCtr++;
              getFiles(fullPath);
            }
            if (dirCtr === 0 && itemCtr === 0) {
              onComplete();
            }
          });
        });
      });
    }

    const metadata = {
      contentType: 'image/jpeg',
    };

    async function onComplete() {
      await Promise.all(
        fileList.map(({ file, name }) => {
          try {
            storageRef.child(name).put(file, metadata);
          } catch (error) {
            console.log(error);
          }
        })
      );

      console.log('files uploaded to successfully');
    }

    getFiles(directoryPath);
  }

  uploadDirectory();
}

upload();
