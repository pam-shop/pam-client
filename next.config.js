/* eslint-disable import/no-extraneous-dependencies ,global-require */
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const withPurgeCss = require('next-purgecss');
const withSass = require('@zeit/next-sass');
const { env } = require('./src/config');

let nextConfig = withSass({
  generateBuildId: async () => process.env.BUILD_ID || 'build',
  webpack(config) {
    const minimizers = config.optimization.minimizer || [];
    minimizers.push(new OptimizeCSSAssetsPlugin({}));

    minimizers.forEach((minimizer) => {
      if (minimizer.options && minimizer.options.terserOptions) {
        // turn on drop_console during build for prod
        minimizer.options.terserOptions = {
          ...minimizer.options.terserOptions,
          compress: {
            drop_console: env !== 'development',
          },
        };
      }
    });

    config.optimization.minimizer = minimizers;
    return {
      ...config,
      module: {
        ...config.module,
        rules: [
          ...config.module.rules,
          {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url-loader',
            options: {
              limit: 10240,
              mimetype: 'image/svg+xml',
              esModule: false,
            },
          },
          {
            test: /\.(png|jpe?g|gif|woff(2)?|ttf|eot)$/,
            use: [
              {
                loader: 'url-loader',
                options: {
                  esModule: false,
                },
              },
            ],
          },
        ],
      },
    };
  },
  cssModules: true,
  cssLoaderOptions: {
    constLoaders: 2,
    localIdentName: '[local]___[hash:base64:5]',
  },
  poweredByHeader: false,
});

if (process.env.PURGECSS) {
  nextConfig = withPurgeCss({
    purgeCssEnabled: ({ dev, isServer }) => !dev && !isServer,
    ...nextConfig,
  });
}

if (process.env.ANALYZE) {
  const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: true,
  });

  nextConfig = withBundleAnalyzer(nextConfig);
}

module.exports = nextConfig;
